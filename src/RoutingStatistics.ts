import _ from 'lodash';
import {
  allApiItems, ApiItemType, completedItems,
  itemCodeToDef,
  zoneItems,
} from './erbs-data/ERBSJsonInterfaces';

export interface NeededBuildItems {
  basicsNeeded: Record<number, number>,
  multiYieldNeeded: Record<number, number>,
  multiYieldExtras: Record<number, number>,
}

function listToRecord(nums: number[]): Record<number, number> {
  const basicsNeeded: Record<number, number> = {};
  nums.forEach((it) => { basicsNeeded[it] = (basicsNeeded[it] || 0) + 1; });
  return basicsNeeded;
}

export default function computeBasicComponentsNeeded(items: number[]): NeededBuildItems {
  const inInventory: number[] = [];
  const needed: number[] = _.cloneDeep(items);
  const basicsNeededList: number[] = [];// for stuff like water
  // for stuff like steel sheets that a team mate might have extras of.
  const multiYieldsNeededList: number[] = [];
  while (needed.length !== 0) {
    const itemCode = needed.pop()!!;
    const inventoryIndex = inInventory.indexOf(itemCode);
    const itemDef = itemCodeToDef[itemCode];
    // if in inventory, remove it
    if (inventoryIndex >= 0) {
      inInventory.splice(inventoryIndex, 1);
      if (itemDef.initialCount > 1) {
        multiYieldsNeededList.push(itemCode);
      }
      if (itemDef.makeMaterial1 === 0) {
        basicsNeededList.push(itemCode);
      }
    } else { // craft or collect the needed item
      for (let i = 0; i < itemDef.initialCount; i += 1) {
        inInventory.push(itemCode);
      }
      if (itemDef.makeMaterial1 !== 0) {
        needed.push(itemDef.makeMaterial2);
        needed.push(itemDef.makeMaterial1);
      }
      needed.push(itemCode);
    }
  }
  return {
    basicsNeeded: listToRecord(basicsNeededList),
    multiYieldNeeded: listToRecord(multiYieldsNeededList),
    multiYieldExtras: listToRecord(inInventory),
  };
}

interface MissingComponentsResult {
  ableToMake: boolean,
  inventoryItemsUsed: number[],
  stillUnableToBuild: number[],
  extraItemsProduced: number[],
  maxZone: number,
}

function zoneIndex(item: number, itemsInZones: Set<number>[]) {
  for (let i = 0; i < itemsInZones.length; i += 1) {
    if (itemsInZones[i].has(item)) {
      return i;
    }
  }
  return -1;
}

function missingComponents(
  item: number, itemsInZones: Set<number>[], inventoryItems: number[],
): MissingComponentsResult {
  // base case
  const itemDef = itemCodeToDef[item];
  if (itemDef.makeMaterial1 === 0) {
    // need to take component first due to max zone concerns
    if (inventoryItems.includes(item)) {
      return {
        ableToMake: true,
        inventoryItemsUsed: [item],
        stillUnableToBuild: [],
        extraItemsProduced: [],
        maxZone: -1,
      };
    }
    const zIndex = zoneIndex(item, itemsInZones);
    // next best case for speed is zone has item
    if (zIndex >= 0) {
      return {
        ableToMake: true,
        inventoryItemsUsed: [],
        stillUnableToBuild: [],
        extraItemsProduced: [],
        maxZone: zIndex,
      };
    }
    // worst case is that it is both not in zones and not in inventory.
    return {
      ableToMake: false,
      inventoryItemsUsed: [],
      stillUnableToBuild: [item],
      extraItemsProduced: [],
      maxZone: zIndex,
    };
  }
  if (inventoryItems.includes(item)) {
    return {
      ableToMake: true,
      inventoryItemsUsed: [item],
      extraItemsProduced: [],
      stillUnableToBuild: [],
      maxZone: -1,
    };
  }
  // if we get here, the item is complex (requires crafting), so we need to see if both
  // components are build-able.
  // going to first check for inventory though since it will reduce search space and I think
  // we always get a correct solution in terms of the basic components still needed.
  // may lead to undesirable effects like thinking we need still sheets because a ruby was made.
  // but improvements for that will be saved for a later date.
  const lhsResult = missingComponents(itemDef.makeMaterial1, itemsInZones, inventoryItems);
  const extraItemsProduced: number[] = [];
  const newInventory = _.cloneDeep(inventoryItems);
  lhsResult.inventoryItemsUsed.forEach((it) => {
    const indexOf = newInventory.indexOf(it);
    newInventory.splice(indexOf, 1);
  });
  const rhsResult = missingComponents(itemDef.makeMaterial2, itemsInZones, newInventory);
  extraItemsProduced.push(...lhsResult.extraItemsProduced);
  extraItemsProduced.push(...rhsResult.extraItemsProduced);
  const ableToMake = lhsResult.ableToMake && rhsResult.ableToMake;
  if (itemDef.initialCount > 1) {
    for (let i = 1; i < itemDef.initialCount; i += 1) {
      extraItemsProduced.push(item);
    }
  }
  const stillUnableToBuild: number[] = [];
  if (!lhsResult.ableToMake) {
    stillUnableToBuild.push(...lhsResult.stillUnableToBuild);
  }
  if (!rhsResult.ableToMake) {
    stillUnableToBuild.push(...rhsResult.stillUnableToBuild);
  }
  return {
    ableToMake,
    inventoryItemsUsed: lhsResult.inventoryItemsUsed.concat(rhsResult.inventoryItemsUsed),
    stillUnableToBuild,
    extraItemsProduced,
    maxZone: Math.max(lhsResult.maxZone, rhsResult.maxZone),
  };
}

export function computeMissingComponents(
  items: number[], route: number[], teammateCanBring: number[],
): MissingComponentsResult {
  const inZones: Set<number>[] = route.map((zoneId) => zoneItems[zoneId]);
  const inventoryRemaining: number[] = _.cloneDeep(teammateCanBring);
  const inventoryUsed: number[] = [];
  const unableToBuild: number[] = [];
  let maxZone = -1;
  items.forEach((itemCode) => {
    const result = missingComponents(itemCode, inZones, inventoryRemaining);
    unableToBuild.push(...result.stillUnableToBuild);
    inventoryUsed.push(...result.inventoryItemsUsed);
    inventoryRemaining.push(...result.extraItemsProduced);
    result.inventoryItemsUsed.forEach((it) => {
      const idx = inventoryRemaining.indexOf(it);
      inventoryRemaining.splice(idx, 1);
    });
    maxZone = Math.max(maxZone, result.maxZone);
  });
  return {
    ableToMake: unableToBuild.length === 0,
    stillUnableToBuild: unableToBuild,
    inventoryItemsUsed: inventoryUsed,
    extraItemsProduced: inventoryRemaining,
    maxZone,
  };
}

export interface BuildableItemResult {
  code: number,
  zoneNumber: number|null,
  zoneNumberWithWeapon: number|null,
}

export function computeBuildableItems(
  zones: Array<number>,
  startingItems: Array<number> = [],
  givenItems: Array<number> = [],
  onlyCompletedItems: boolean = true,
  allowWeaponsForNonWeapons: boolean = false,
): Array<BuildableItemResult> {
  const possible = [] as BuildableItemResult[];
  const limitedItems = givenItems.concat(startingItems);
  allApiItems.map((item) => {
    if (onlyCompletedItems && !completedItems.has(item.code)) {
      return {
        item,
        latestZoneWithItems: null,
        latestZoneWithItemOrWeapon: null,
      };
    }
    let latestZoneWithItems: number|null = null;
    const missingWithoutGiven = computeMissingComponents([item.code], zones, []);
    if (missingWithoutGiven.ableToMake) {
      latestZoneWithItems = missingWithoutGiven.maxZone + 1;
    }
    let latestZoneWithItemOrWeapon: number|null = null;
    const missingWithGiven = computeMissingComponents([item.code], zones, limitedItems);
    if (missingWithGiven.ableToMake) {
      latestZoneWithItemOrWeapon = missingWithGiven.maxZone + 1;
    }
    return {
      item,
      latestZoneWithItems,
      latestZoneWithItemOrWeapon,
    };
  })
    .filter((it) => (
      (allowWeaponsForNonWeapons || it.item.itemType === ApiItemType.Weapon)
      && it.latestZoneWithItemOrWeapon != null
    )
      || it.latestZoneWithItems != null)
    .forEach((item) => {
      possible.push({
        code: item.item.code,
        zoneNumber: item.latestZoneWithItems,
        zoneNumberWithWeapon: item.latestZoneWithItemOrWeapon,
      });
    });
  return possible;
}
