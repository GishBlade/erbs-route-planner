import React, { useEffect } from 'react';

export default function usePersistedState<T>(key: string, defaultVal: T): [T, (value: T) => void] {
  const [state, setState] = React.useState(
    () => {
      const localGet = localStorage.getItem(key);
      if (localGet != null) {
        return JSON.parse(localGet) as T;
      }
      return defaultVal;
    },
  );
  useEffect(() => {
    localStorage.setItem(key, JSON.stringify(state));
  }, [key, state]);
  return [state, setState];
}
