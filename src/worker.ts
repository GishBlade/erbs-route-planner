import {
  advancedMultiItemRouting,
  AdvancedRoutingConfig,
} from './route-generation/AdvancedRouteGeneration';

// eslint-disable-next-line no-restricted-globals
const ctx: Worker = self as any;

export default class AdvancedRoutingWorker {
  // eslint-disable-next-line class-methods-use-this
  calculate(routingInfo: [number[], number[], number, AdvancedRoutingConfig, string[]]) {
    return advancedMultiItemRouting(...routingInfo);
  }
}

const thingy = new AdvancedRoutingWorker();

ctx.addEventListener('message', (event) => {
  const result = thingy.calculate(event.data);
  ctx.postMessage(result);
});
