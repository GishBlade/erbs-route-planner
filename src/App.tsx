/* eslint-disable max-len,react/no-unescaped-entities */
import React, { useEffect, useState } from 'react';
import './App.css';
import {
  Box,
  createMuiTheme, Divider,
  FormControl,
  Grid, InputLabel, Link,
  makeStyles, MenuItem,
  MuiThemeProvider, Select,
  Tab, Tabs, Toolbar,
  Typography,
} from '@material-ui/core';
import { NotificationContainer } from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import _, { min, parseInt } from 'lodash';
import { LumiaIslandMap } from './lumia-island-map/LumiaIslandMap';
import { BuildableItemCard } from './components/BuildableItemCard';
import {
  weaponInfo,
  WeaponType,
  defaultStartingItems, universalCollectables, zoneIdToName,
} from './erbs-data/ERBSJsonInterfaces';
import { characterColors } from './erbs-data/MiscInfo';
import {
  BuildSelector,
  buildSelectorMarginAmount,
  buildSelectorWidth,
  CharacterRoutingInfo,
} from './components/BuildSelector';
import { computeBuildableItems } from './RoutingStatistics';
import {
  advancedMultiItemRouting,
  defaultAdvancedRoutingConfig,
  HasInventory,
} from './route-generation/AdvancedRouteGeneration';
import { ItemIcon, itemIconWidth } from './components/ItemIcon';
import { RoutingAlgorithm } from './route-generation/RouteGenerationLib';
import { ComponentHelp } from './components/ComponentHelp';
import { ModalButton } from './components/ModalButton';
import WhatsNewButton from './WhatsNewButton';
import { UPDATE_TIME } from './update-time';

// this overrides default values for components from the material-ui library
const theme = createMuiTheme({
  palette: {
    type: 'dark',
  },
  props: {
    MuiButton: {
      variant: 'outlined',
    },
  },
  overrides: {
    MuiAccordion: {
      root: {
        // width: '100%',
        width: '100%',
        margin: 0,
        '&$expanded': {
          margin: 0,
        },
      },
    },
    MuiAccordionSummary: {
      root: {
        paddingTop: 0,
        paddingBottom: 0,
        margin: 0,
        '&$expanded': {
          margin: 0,
        },
      },
    },
    MuiAccordionDetails: {
      root: {
        // padding: 3,
        paddingTop: 3,
        paddingBottom: 3,
        margin: 0,
      },
    },
    MuiCard: {
      root: {
        padding: 5,
        margin: 0,
      },
    },
    MuiButton: {
      root: {
        marginRight: 3,
        marginLeft: 3,
      },
      text: {
        padding: 1,
        margin: 1,
      },
    },
    MuiTextField: {
      root: {
        width: '99%',
      },
    },
  },
});

const useStyles = makeStyles({
  root: {
    'text-align': 'left',
    'vertical-align': 'text-top',
    width: '100%',
    height: '100%',
  },
  appbarGridItem: {
    marginLeft: 5,
    marginRight: 5,
  },
  progressBar: {
    // height: 15,
  },
});

function defaultRouteInfo(): CharacterRoutingInfo {
  return {
    selectedRoute: [],
    startingWeapons: [],
    weaponFilter: [],
    teammateBringsItems: [],
    selectedItems: [],
    advancedRoutingConfig: defaultAdvancedRoutingConfig,
  };
}

// ty https://stackoverflow.com/questions/36862334/get-viewport-window-height-in-reactjs
function getWindowDimensions() {
  const { innerWidth: width, innerHeight: height } = window;
  return {
    width,
    height,
  };
}

function useWindowDimensions() {
  const [windowDimensions, setWindowDimensions] = useState(getWindowDimensions());

  useEffect(() => {
    function handleResize() {
      setWindowDimensions(getWindowDimensions());
    }

    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, []);

  return windowDimensions;
}

function App() {
  const debugMode = false;
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const classes = useStyles();
  const [activeCharacter, setActiveCharacter] = useState(0 as number);
  const [routingInfo, setRoutingInfo] = useState([
    defaultRouteInfo(),
  ] as CharacterRoutingInfo[]);
  const [currentMode, setCurrentMode] = useState(0);
  const [selectedInfoTab, setSelectedInfoTab] = useState(0);
  const { width } = useWindowDimensions();
  function setMode(numCharacters) {
    setActiveCharacter(Math.min(activeCharacter, numCharacters - 1));
    if (routingInfo.length > numCharacters) {
      setRoutingInfo(routingInfo.slice(0, numCharacters));
    } else {
      const newThingy = _.cloneDeep(routingInfo);
      while (newThingy.length < numCharacters) {
        newThingy.push(defaultRouteInfo());
      }
      setRoutingInfo(newThingy);
    }
    setCurrentMode(numCharacters - 1);
  }
  // const [allowWeaponForNonWeapon, setAllowWeaponForNonWeapon] = useState(debugMode);
  const allowWeaponForNonWeapon = true;
  const [routingAlgorithm, setRoutingAlgorithm] = useState('advanced' as RoutingAlgorithm);
  const activeRoutingInfo = routingInfo[activeCharacter];
  const {
    startingWeapons,
    selectedRoute,
    teammateBringsItems,
    weaponFilter,
    selectedItems,
    advancedRoutingConfig,
  } = activeRoutingInfo;
  const [inventoryStates, setInventoryStates] = useState([] as HasInventory[]);
  const [inventoryStateZoneNums, setInventoryStateZoneNums] = useState([] as number[]);
  useEffect(() => {
    if (selectedRoute.length > 0 && selectedItems.length > 0) {
      const routeResult = advancedMultiItemRouting(
        selectedItems,
        defaultStartingItems.concat(startingWeapons.map((it) => weaponInfo[it].startingWeapon)),
        1,
        {
          ...advancedRoutingConfig,
          inventoryProtection: false,
          zoneConstraints: selectedRoute.map((it) => [it]),
          maxZones: selectedRoute.length,
          returnIfMax: true,
          allZonesHaveTP: true,
          ignoreNoValueZones: false,
        },
        weaponFilter,
      );
      if (routeResult.length === 1) {
        const routingStates = routeResult[0].states;
        setInventoryStates(routingStates);
        setInventoryStateZoneNums(routeResult[0].zones);
      }
    } else {
      setInventoryStates([]);
      setInventoryStateZoneNums([]);
    }
  }, [advancedRoutingConfig, startingWeapons, selectedItems, weaponFilter, selectedRoute]);
  const weaponTypeFilter = weaponFilter;

  const showBothTabs = currentMode <= 0 && width >= 1800;
  const lhsGap = 5;
  const lhsSingleColumnWidth = 400;
  const lhsColumnWidths = (lhsSingleColumnWidth) * (1 + (showBothTabs ? 1 : 0)) + (showBothTabs ? lhsGap : 0);
  const totalCharacterColumnSize = (1 + currentMode) * (buildSelectorWidth + (2 * buildSelectorMarginAmount));
  const mapMargin = 13;
  const anUnknownFlatAmount = 30;// This does not work with just 10. Works with 30 so not touching anymore.
  // I was not able to re-produce the issue locally, but a user was able to test and going from 10->30 fixed it.
  const remainingWidthForMap = min([width - lhsColumnWidths - totalCharacterColumnSize - (2 * mapMargin) - anUnknownFlatAmount, 700]);

  function indexedReplace<T>(original: T[], index: number, newVal: T): T[] {
    return original.map((val, idx) => (idx === index ? newVal : val));
  }
  const newBuildableItems = computeBuildableItems(
    selectedRoute,
    defaultStartingItems,
    startingWeapons.map((used) => weaponInfo[WeaponType[used]].startingWeapon)
      .concat(teammateBringsItems),
    false,
    allowWeaponForNonWeapon,
  );
  const lhsStyle = { width: 400 };
  const buildableItemsComponentHelp = (
    <ComponentHelp
      iconSize="small"
      title={(
        <>
          <Typography>
            The zone the item can be built is in the top right.
          </Typography>
          <Typography>
            Red means limited quantity items (starting weapon/items or teammate/rng items)
            {' '}
            must be used.
          </Typography>
          <Typography>
            Blue means all items can be looted from zones.
          </Typography>
          <Typography>
            Red and X/Y means it can be built with limited quantity items
            {' '}
            in zone X, or without it by zone Y.
          </Typography>
        </>
      )}
    />
  );
  return (
    <div className={classes.root}>
      <NotificationContainer />
      <header className="App-header">
        <MuiThemeProvider theme={theme}>
          <Toolbar>
            <Grid container direction="row">
              <Grid item className={classes.appbarGridItem}>
                <Typography variant="h3" align="left">Gishblade's Router</Typography>
              </Grid>
              <Grid item className={classes.appbarGridItem}>
                <Divider orientation="vertical" />
              </Grid>
              <Grid item className={classes.appbarGridItem}>
                <Typography align="center" className={classes.appbarGridItem} style={{ marginTop: 5 }}>
                  API Data Updated
                  {' '}
                  {UPDATE_TIME}
                </Typography>
              </Grid>
              <Grid item className={classes.appbarGridItem}>
                <Divider orientation="vertical" />
              </Grid>
              <Grid item className={classes.appbarGridItem}>
                <FormControl style={{ width: 200 }}>
                  <InputLabel>Route Generation Method</InputLabel>
                  <Select value={routingAlgorithm} onChange={(it) => setRoutingAlgorithm(it.target.value as RoutingAlgorithm)}>
                    <MenuItem
                      value="simple"
                      title="Just tries to find a route with the needed components. Does not do any kind of inventory simulation"
                    >
                      Simple
                    </MenuItem>
                    <MenuItem
                      value="advanced"
                      title="Performs some Inventory simulation to ensure the routing works. Additionally prioritizes finishing weapon."
                    >
                      Advanced
                    </MenuItem>
                  </Select>
                </FormControl>
                <ComponentHelp
                  maxWidth="lg"
                  title={<Typography>Click this to find out more about the routing algorithms</Typography>}
                  dialogHelp={(
                    <Box>
                      <Typography variant="h4">Simple</Typography>
                      <Typography>
                        This method only looks to see that all the needed basic components can be found.
                      </Typography>
                      <Typography variant="h6">Pros</Typography>
                      <ul style={{ marginTop: 0, marginBottom: 0 }}>
                        <li>Fast</li>
                        <li>Can be used in a hacky way to guess at joint duos/squads routing.</li>
                        <ul>
                          <li>Just keep adding items to a solos build and you'll at least find out a set of zones that might work.</li>
                        </ul>
                      </ul>
                      <Typography variant="h6">Cons and Limitations</Typography>
                      <ul style={{ marginTop: 0, marginBottom: 0 }}>
                        <li>Does not simulate inventory, so may suggest unusable routes.</li>
                        <li>Does not put any priority on finishing weapon.</li>
                      </ul>
                      <Typography variant="h4">Advanced v1.0</Typography>
                      <Typography>
                        This method currently simulates inventory along the way so builds are much more do-able.
                      </Typography>
                      <Typography>
                        There's also some other things (mentioned below) that make this generate better solos routes.
                      </Typography>
                      <Typography variant="h6">Pros</Typography>
                      <ul style={{ marginTop: 0, marginBottom: 0 }}>
                        <li>Will ensure you have enough space between zones for the route.</li>
                        <li>Makes completing weapon a priority.</li>
                      </ul>
                      <Typography variant="h6">Cons and Limitations</Typography>
                      <ul style={{ marginTop: 0, marginBottom: 0 }}>
                        <li>With the exception of branches/rocks, assumes you MUST collect a basic components as soon as it's available (including leather)</li>
                        <li>Assumes all characters can get leather in zone 2+ (even if weapon is not done).</li>
                        <li>Leather can only be gotten from chicken, dogs, and wolves.</li>
                        <li>Does not make sure enough time/zones has passed for wolves to have spawned</li>
                        <li>Does not do any checks for in-zone inventory congestion (only between-zone issues are detected).</li>
                        <li>Probably more that I'm forgetting</li>
                      </ul>
                    </Box>
                  )}
                />
              </Grid>
              <Grid item className={classes.appbarGridItem}>
                <Divider orientation="vertical" />
              </Grid>
              <Grid item>
                <Typography>
                  Map too small for squads mode?
                </Typography>
                <Typography>
                  Try zooming out (not in).
                </Typography>
              </Grid>
              <Grid item className={classes.appbarGridItem}>
                <Divider orientation="vertical" />
              </Grid>
              <Grid item style={{ marginTop: 3 }}>
                <ModalButton
                  buttonText="Report Bug"
                  modal={(
                    <Box style={{ width: 600, padding: 10 }}>
                      <Typography align="left">
                        DM Gishblade#0003 on Discord for bug reports if you don't see a
                        {' '}
                        <Link style={{ color: 'hotpink' }} href="https://gitlab.com/GishBlade/erbs-route-planner/-/issues" target="_blank">Gitlab Issue</Link>
                        {' '}
                        for it.
                      </Typography>
                      <Typography>
                        The best bug reports include:
                      </Typography>
                      <Typography>1. A description of the bug</Typography>
                      <Typography>2. Steps to reproduce the issue</Typography>
                      <Typography>3. Screenshot(s)</Typography>
                    </Box>
                  )}
                />
              </Grid>
              <Grid item style={{ marginTop: 3 }}>
                <ModalButton
                  buttonText="Source Code"
                  modal={(
                    <Box style={{ width: 600, padding: 10 }}>
                      <Typography align="left">
                        If you'd like to look at the code
                        {' '}
                        <Link style={{ color: 'hotpink' }} href="https://gitlab.com/GishBlade/erbs-route-planner" target="_blank">check out the GitLab page</Link>
                        .
                      </Typography>
                      <Typography>
                        If you're interested in helping out, DM Gishblade#0003 on Discord.
                      </Typography>
                    </Box>
                  )}
                />
              </Grid>
              <Grid item>
                <WhatsNewButton />
              </Grid>
            </Grid>
          </Toolbar>
          <Divider />
          <Grid container justify="center">
            <Grid item key="bahblah">
              <Grid container>
                <Grid item key="bahblah-2" style={{ minWidth: lhsSingleColumnWidth, maxWidth: lhsSingleColumnWidth * (showBothTabs ? 2 : 1) + (showBothTabs ? lhsGap : 0) }}>
                  {showBothTabs ? '' : (
                    <Typography variant="h5">
                      Stuff for Character
                      {' '}
                      {activeCharacter + 1}
                    </Typography>
                  )}
                  <Grid container direction={!showBothTabs ? 'column' : 'row'}>
                    {showBothTabs ? '' : (
                      <Grid item key="actual-tab-selector" style={{ marginBottom: 10 }}>
                        <Tabs value={selectedInfoTab} onChange={(event, newValue) => setSelectedInfoTab(newValue)}>
                          <Tab label={(
                            <Box>
                              <Typography display="inline">
                                Build-able
                              </Typography>
                              {buildableItemsComponentHelp}
                            </Box>
                          )}
                          />
                          <Tab label="Inventory Sim" />
                        </Tabs>
                      </Grid>
                    )}
                    {!(selectedInfoTab === 0 || showBothTabs) ? '' : (
                      <Grid container style={{ minWidth: 400, maxWidth: 400 }}>
                        {!showBothTabs ? '' : (
                          <Grid item>
                            <Typography variant="h5" display="inline" style={{ marginRight: 5 }}>
                              Build-able Items
                            </Typography>
                            {buildableItemsComponentHelp}
                          </Grid>
                        )}
                        <Grid item>
                          <Box style={lhsStyle}>
                            <BuildableItemCard
                              width={383}
                              weaponTypes={weaponTypeFilter as WeaponType[]}
                              items={newBuildableItems}
                              debugMode={debugMode}
                            />
                          </Box>
                        </Grid>
                      </Grid>
                    )}
                    {!(selectedInfoTab === 1 || showBothTabs) ? '' : (
                      <Grid
                        container
                        style={{ width: 400, marginLeft: (showBothTabs ? lhsGap : 0) }}
                        alignContent="flex-start"
                      >
                        {!showBothTabs ? '' : (
                          <Grid item>
                            <Typography variant="h5">
                              Inventory Sim
                            </Typography>
                          </Grid>
                        )}
                        <Grid item>
                          <Typography>
                            (Assumes Team/Rare items in 3rd zone.)
                          </Typography>
                        </Grid>
                        {inventoryStates.map(({ inventory, basicComponentsTaken }, index) => {
                          const zoneName = zoneIdToName[inventoryStateZoneNums[index]];
                          return (
                            <>
                              <Grid
                                item
                                style={{
                                  marginBottom: 10,
                                  outlineStyle: 'solid',
                                  outlineColor: 'gray',
                                  borderRadius: 5,
                                  outlineWidth: 1,
                                }}
                              >
                                <Box style={lhsStyle}>
                                  <Typography variant="h5" style={{ marginLeft: 2 }}>
                                    {zoneName}
                                  </Typography>
                                  <Grid container direction="row" style={{ paddingLeft: 2 }}>
                                    {Object.entries(basicComponentsTaken).map((it) => (
                                      <Grid item>
                                        <ItemIcon item={parseInt(it[0], 10)} blueBadgeNumber={it[1]} />
                                      </Grid>
                                    ))}
                                  </Grid>
                                  <Divider />
                                  <Grid container direction="row" style={{ paddingLeft: 2 }}>
                                    <Grid item>
                                      <Grid container direction="row" style={{ width: itemIconWidth * 3 }}>
                                        <Grid item><ItemIcon item={inventory.weapon || 0} /></Grid>
                                        <Grid item><ItemIcon item={inventory.chest || 0} /></Grid>
                                        <Grid item><ItemIcon item={inventory.head || 0} /></Grid>
                                        <Grid item><ItemIcon item={inventory.arm || 0} /></Grid>
                                        <Grid item><ItemIcon item={inventory.leg || 0} /></Grid>
                                        <Grid item><ItemIcon item={inventory.trinket || 0} /></Grid>
                                      </Grid>
                                    </Grid>
                                    <Grid item>
                                      <Divider orientation="vertical" />
                                    </Grid>
                                    <Grid item>
                                      <Grid container direction="row" style={{ width: ((itemIconWidth) * 5) }}>
                                        {inventory.inventory.filter(
                                          (it) => !universalCollectables.includes(it.code),
                                        ).map((it, idxThingy) => (
                                          <Grid item key={`inv-zone-${zoneName}-item-code-${it.code}-${activeCharacter}-${idxThingy + 1337}-${activeCharacter}`}>
                                            <ItemIcon
                                              item={it.code}
                                              blueBadgeNumber={it.amount}
                                            />
                                          </Grid>
                                        ))}
                                      </Grid>
                                    </Grid>
                                  </Grid>
                                </Box>
                              </Grid>
                            </>
                          );
                        })}
                      </Grid>
                    )}
                  </Grid>
                </Grid>
                <Grid item key="map-grid-item">
                  <Typography variant="h4" align="center" style={{ maxWidth: remainingWidthForMap }}>
                    Routing for Character
                    {' '}
                    {activeCharacter + 1}
                  </Typography>
                  <LumiaIslandMap
                    activeCharacter={activeCharacter}
                    refName="mappy"
                    maxWidth={remainingWidthForMap}
                    selectedZoneIds={routingInfo.map((it) => it.selectedRoute)}
                    setSelectedZoneIds={(zoneIds) => {
                      const newPlayerThingy = routingInfo.map(
                        (it, index) => ({ ...it, selectedRoute: zoneIds[index] }),
                      );
                      setRoutingInfo(newPlayerThingy);
                    }}
                  />
                </Grid>
                <Grid item key="build-section-with-tabs">
                  <Grid container direction="column">
                    <Grid item key="actual-tab-selector">
                      <Tabs
                        value={currentMode}
                        onChange={(event, newValue) => setMode(newValue + 1)}
                      >
                        <Tab style={{ width: 75, minWidth: 75 }} label="Solos" />
                        <Tab style={{ width: 75, minWidth: 75 }} label="Duos" />
                        <Tab style={{ width: 75, minWidth: 75 }} label="Squads" />
                      </Tabs>
                    </Grid>
                    <Grid item key="the tabs themselves">
                      <Grid container direction="row" style={{ width: totalCharacterColumnSize }}>
                        {routingInfo.map((routeInfo, index) => {
                          const characterNumber = index + 1;
                          const gridItemKey = `build-selector-${(index + 7) * (characterNumber + 37)}`;
                          return (
                            <Grid item key={gridItemKey}>
                              <Typography variant="h4">
                                Character
                                {' '}
                                {characterNumber}
                              </Typography>
                              <BuildSelector
                                currentlyActive={activeCharacter}
                                setActiveCharacter={setActiveCharacter}
                                outlineColor={characterColors[index]}
                                routingInfo={routeInfo}
                                setRoutingInfo={(it) => setRoutingInfo(indexedReplace(routingInfo, index, it))}
                                characterIndex={index}
                                allRoutingInfo={routingInfo}
                                routingAlgorithm={routingAlgorithm}
                              />
                            </Grid>
                          );
                        })}
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </MuiThemeProvider>
      </header>
    </div>
  );
}

export default App;
