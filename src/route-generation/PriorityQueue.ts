export interface Comparable {
  compareValue: number,
}

export class PriorityQueue<T extends Comparable> {
  elements = [] as T[];

  size(): number {
    return this.elements.length;
  }

  isNotEmpty() {
    return this.elements.length > 0;
  }

  poll(): T|null {
    if (this.elements.length === 0) {
      return null;
    }
    const toReturn = this.elements[0];
    if (this.elements.length > 1) {
      // noinspection UnnecessaryLocalVariableJS
      const toPush = this.elements.pop() as T;
      this.elements[0] = toPush;
      this.heapifyDown(0);
    } else {
      this.elements.pop();
    }
    return toReturn;
  }

  heapifyDown(dex: number) {
    if (dex >= this.elements.length) {
      return;
    }
    const leftChild = dex * 2 + 1;
    const rightChild = dex * 2 + 2;
    if (rightChild < this.elements.length) {
      // both are valid children
      if (this.elements[dex].compareValue > this.elements[leftChild].compareValue
        || this.elements[dex].compareValue > this.elements[rightChild].compareValue) {
        if (this.elements[leftChild].compareValue < this.elements[rightChild].compareValue) {
          this.swap(dex, leftChild);
          this.heapifyDown(leftChild);
        } else {
          this.swap(dex, rightChild);
          this.heapifyDown(rightChild);
        }
      }
    } else if (leftChild < this.elements.length) {
      // if this happens, we know right child is not < elements.size
      if (this.elements[dex].compareValue > this.elements[leftChild].compareValue) {
        this.swap(dex, leftChild);
        this.heapifyDown(leftChild);
      }
    }
  }

  add(element: T) {
    // console.log('adding', element);
    this.elements.push(element);
    this.heapifyUp(this.elements.length - 1);
  }

  swap(a: number, b: number) {
    const tmp = this.elements[a];
    this.elements[a] = this.elements[b];
    this.elements[b] = tmp;
  }

  heapifyUp(dex: number) {
    if (dex === 0) {
      return;
    }
    const parent = Math.floor((dex - 1) / 2);
    if (this.elements[parent].compareValue > this.elements[dex].compareValue) {
      this.swap(parent, dex);
      this.heapifyUp(parent);
    }
  }
}
