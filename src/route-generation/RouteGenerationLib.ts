import { Comparable, PriorityQueue } from './PriorityQueue';
import { zoneLookupTable } from '../erbs-data/ERBSJsonInterfaces';

export interface RoutingState<T extends RoutingState<T>> {
  zone: number,
  parent: T|null,
  numZones: number,
}

export type RoutingAlgorithm = 'simple'|'advanced';

function mapToZoneIds(names: string[]): number[] {
  return names.map((it) => zoneLookupTable[it]);
}

const zoneAdjacency: [boolean, number[]][] = [
  [false, mapToZoneIds([])], // not a zone. here because zone IDs start at 1....
  [false, mapToZoneIds(['Uptown', 'Chapel', 'Factory'])], // dock
  [false, mapToZoneIds(['Avenue', 'Temple', 'Hospital'])], // Pond
  [false, mapToZoneIds(['Hotel', 'Forest', 'Uptown'])], // Beach
  [true, mapToZoneIds(['Beach', 'Forest', 'Chapel', 'Dock'])], // Uptown
  [true, mapToZoneIds(['Archery Range', 'School', 'Avenue', 'Temple'])], // Alley
  [true, mapToZoneIds(['Beach', 'Forest', 'School', 'Archery Range'])], // Hotel
  [true, mapToZoneIds(['Alley', 'School', 'Temple', 'Pond'])], // Avenue
  [true, mapToZoneIds(['Pond', 'Cemetery', 'Factory'])], // Hospital
  [true, mapToZoneIds(['Alley', 'Avenue', 'Pond'])], // Temple
  [false, mapToZoneIds(['School', 'Hotel', 'Alley'])], // Archery Range
  [true, mapToZoneIds(['Pond', 'Hospital', 'Factory', 'Chapel'])], // Cemetery
  [false, mapToZoneIds(['Hotel', 'School', 'Avenue', 'Chapel', 'Uptown', 'Beach'])], // Forest
  [true, mapToZoneIds(['Hospital', 'Cemetery', 'Chapel', 'Dock'])], // Factory
  [false, mapToZoneIds(['Forest', 'Cemetery', 'Factory', 'Dock', 'Uptown'])], // Chapel
  [false, mapToZoneIds(['Archery Range', 'Hotel', 'Forest', 'Avenue', 'Alley'])], // School
];

export function toZoneNumbers<T extends RoutingState<T>>(routes: ComparablePair<T>): number[] {
  let currentState: T|null = routes.state;
  const toReturn: number[] = [];
  while (currentState != null) {
    toReturn.push(currentState.zone);
    currentState = currentState.parent;
  }
  return toReturn.reverse();
}

export function toStateArray<T extends RoutingState<T>>(routes: ComparablePair<T>): T[] {
  let currentState: T|null = routes.state;
  const toReturn: T[] = [];
  while (currentState != null) {
    toReturn.push(currentState);
    currentState = currentState.parent;
  }
  return toReturn.reverse();
}

const primesNeeded = 16 * 16;
const primes: number[] = [2];
let nextToCheck = 3;
while (primes.length < primesNeeded) {
  let anyFactors = false;
  const num = nextToCheck;
  primes.forEach((it) => {
    if (num % it === 0) {
      anyFactors = true;
    }
  });
  if (!anyFactors) {
    primes.push(nextToCheck);
  }
  nextToCheck += 2;
}

export function zoneNumsToKey(zones: number[], orderMatters: boolean = false): number {
  let result = 1;
  let zoneNum = 0;
  zones.forEach((it) => {
    if (orderMatters) {
      result *= primes[it + 10 * zoneNum];
    } else {
      result *= primes[it];
    }
    zoneNum += 1;
  });
  return result;
}

export function toZoneNumbersFromState<T extends RoutingState<T>>(state: T): number[] {
  let currentState: T|null = state;
  const toReturn: number[] = [];
  while (currentState != null) {
    toReturn.push(currentState.zone);
    currentState = currentState.parent;
  }
  return toReturn.reverse();
}

const zoneIds = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];

export interface ComparablePair<T extends RoutingState<T>> extends Comparable {
  state: T
  cost: number,
  heuristic: number,
}

function hasNotVisited<T extends RoutingState<T>>(state: RoutingState<T>|null, edge: number) {
  if (state == null) {
    return true;
  }
  return edge !== state.zone && hasNotVisited(state.parent, edge);
}

function processEdges<P extends RoutingState<P>>(
  edgeList: number[],
  currentState: P,
  polledPair: ComparablePair<P>,
  eCost: number,
  expand: (previousState: P|null, zoneId: number) => [P|null, number],
  heuristic: (state: P) => number,
  q: PriorityQueue<ComparablePair<P>>,
  routeDupCode: (state: RoutingState<P>) => number,
  approvedRoutes: Set<number>,
) {
  edgeList.forEach((edge) => {
    if (hasNotVisited(currentState, edge)) {
      const [child, cost] = expand(currentState, edge);
      if (child != null) {
        const h = heuristic(child);
        const element = {
          state: child,
          cost: cost + polledPair.cost + eCost,
          heuristic: h,
          compareValue: cost + polledPair.cost + eCost + h,
        };
        const routeCode = routeDupCode(element.state);
        if (!approvedRoutes.has(routeCode)) {
          q.add(element);
        }
      }
    }
  });
}

export function aStartSearch<P extends RoutingState<P>>(
  expand: (previousState: P|null, zoneId: number) => [P|null, number],
  targetTest: (state: P) => boolean,
  numToReturn: number,
  // default to admissible heuristic
  heuristic: (state: P) => number = () => 0,
  maxZones: number = 6,
  edgeCost: number = 5,
  teleportCost: number = 5,
  routeDupCode: (state: RoutingState<P>) => number,
  allZonesHaveTp: boolean = false,
): ComparablePair<P>[] {
  const toReturn = [] as ComparablePair<P>[];
  const q = new PriorityQueue<ComparablePair<P>>();
  zoneIds.forEach((zoneId) => {
    const [state, cost] = expand(null, zoneId);
    if (state != null) {
      const heuristicCost = heuristic(state);
      const element = {
        state,
        cost,
        heuristic: heuristicCost,
        compareValue: cost + heuristicCost,
      } as ComparablePair<P>;
      q.add(element);
    }
  });

  const approvedRoutes: Set<number> = new Set();
  while (q.isNotEmpty() && toReturn.length < numToReturn) {
    const polled: ComparablePair<P> | null = q.poll();
    if (polled == null) {
      return toReturn;
    }
    // if cost is zero, then no items were taken in this zone.
    // if (polled.cost !== 0) {
    const { state } = polled;
    const routeCode = routeDupCode(polled.state);
    if (approvedRoutes.has(routeCode)) {
      // do nothing
    } else if (targetTest(state)) {
      approvedRoutes.add(routeCode);
      toReturn.push(polled);
    } else if (state.numZones >= maxZones) {
      // do nothing
    } else {
      const [hasTeleport, edges] = zoneAdjacency[state.zone];
      if (!allZonesHaveTp && (!hasTeleport || teleportCost > edgeCost)) {
        processEdges(
          edges, state, polled, edgeCost, expand, heuristic, q, routeDupCode, approvedRoutes,
        );
      }
      if (hasTeleport || allZonesHaveTp) {
        processEdges(
          zoneIds,
          state, polled, teleportCost, expand, heuristic, q, routeDupCode, approvedRoutes,
        );
      }
    }
  }
  return toReturn;
}
