import {
  aStartSearch,
  ComparablePair,
  RoutingState,
  toZoneNumbers,
  toZoneNumbersFromState,
  zoneNumsToKey,
} from './RouteGenerationLib';
import { basicComponentsNeeded, zoneItems } from '../erbs-data/ERBSJsonInterfaces';
import { singleRouteStartingItems } from '../erbs-data/MiscInfo';

interface BasicRoutingState extends RoutingState<BasicRoutingState> {
  neededItems: number[],
  startingItemsRemaining: number[],
}

const baseItemCost = 7;

export function multiItemRouting(
  targetItemCodes: number[],
  startingItems: number[],
  numToReturn: number,
): number[][] {
  const itemsToGet = targetItemCodes.flatMap(
    (targetItemCode) => basicComponentsNeeded[targetItemCode],
  );
  const result = aStartSearch(
    (previousState, zoneId) => {
      const allItemsNeeded = previousState?.neededItems || itemsToGet;
      let startingItemsRemaining = previousState?.startingItemsRemaining || startingItems;
      const stillNeeded = [] as number[];
      const currentZoneNum = (previousState?.numZones || 0) + 1;
      let itemCosts = 0;
      const currentZoneItems = zoneItems[zoneId];
      allItemsNeeded.forEach((needed) => {
        if (startingItemsRemaining.includes(needed)) {
          // do nothing. we don't need to get it
          const idx = startingItemsRemaining.indexOf(needed);
          startingItemsRemaining = startingItemsRemaining
            .filter((ignored, index) => index !== idx);
        } else if (currentZoneItems.has(needed)) {
          // itemCosts += baseItemCost + currentZoneNum;
          itemCosts += baseItemCost;
        } else {
          stillNeeded.push(needed);
        }
      });
      return [{
        neededItems: stillNeeded,
        zone: zoneId,
        startingItemsRemaining,
        parent: previousState,
        numZones: currentZoneNum,
      }, itemCosts];
    },
    (state) => state.neededItems.length === 0,
    numToReturn,
    (state) => state.neededItems.length * (baseItemCost),
    6,
    5,
    5,
    (state) => zoneNumsToKey(toZoneNumbersFromState(state)),
  ) as ComparablePair<BasicRoutingState>[];
  return result.map((it) => toZoneNumbers(it));
}

export function singleItemRouting(
  targetItemCode: number,
  startingItems: number[],
  numToReturn: number,
): number[][] {
  return multiItemRouting([targetItemCode], startingItems, numToReturn);
}

const singleRouteCache: Record<number, number[][]> = {};

export function cachedSingleItemRouting(targetItemCode: number) {
  if (singleRouteCache[targetItemCode] != null) {
    return singleRouteCache[targetItemCode];
  }
  const temp = singleItemRouting(targetItemCode, singleRouteStartingItems, 5);
  singleRouteCache[targetItemCode] = temp;
  return temp;
}
