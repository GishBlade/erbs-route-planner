import _ from 'lodash';
import {
  aStartSearch,
  ComparablePair,
  RoutingState,
  toStateArray,
  toZoneNumbers,
  toZoneNumbersFromState,
  zoneNumsToKey,
} from './RouteGenerationLib';
import {
  ApiArmorType,
  ApiItem,
  ApiItemType,
  itemCodeToDef,
  universalCollectables,
  WeaponType,
  zoneExpectedItemCounts,
  zoneItems,
} from '../erbs-data/ERBSJsonInterfaces';

interface InventoryStack {
  code: number,
  amount: number,
  maxStacks: number,
  itemType: ApiItemType,
  weaponType: WeaponType|null,
}

function makeInventoryStack(code: number, amount: number): InventoryStack {
  const itemDef = itemCodeToDef[code];
  return {
    code,
    amount,
    maxStacks: itemDef.stackable,
    itemType: itemDef.itemType,
    weaponType: itemDef.weaponType,
  };
}

interface InventoryState {
  weapon: number|null,
  head: number|null,
  chest: number|null,
  arm: number|null,
  leg: number|null,
  trinket: number|null,
  inventory: InventoryStack[],
}

interface CombineRule {
  code: number,
  material1: number,
  material2: number,
  amountProduced: number,
}

interface NeededComponentsCombinesResult {
  basicComponentsNeeded: InventoryStack[],
  combinesNeeded: CombineRule[],
  startingInventoryNeeded: InventoryStack[],
}

function computeNeededComponentsAndCombines(
  targets: number[],
  startingInventory: number[],
  teamMateItems: number[],
): NeededComponentsCombinesResult {
  const bComponentsNeeded: InventoryStack[] = [];
  const combinesNeeded: CombineRule[] = [];
  const startingInventoryNeeded: InventoryStack[] = [];
  const startingInventoryLeft: number[] = _.cloneDeep(startingInventory);
  const tempInventory: number[] = [];
  const teamRareLeft = _.clone(teamMateItems);
  const needToBuild: ApiItem[] = targets.map((it) => itemCodeToDef[it]);
  while (needToBuild.length > 0) {
    const itemDef = needToBuild.pop()!!;
    // cases
    // 1. item was already made from a multi-yield recipe, or from starting items
    const startIdx = startingInventoryLeft.indexOf(itemDef.code);
    const invIdx = tempInventory.indexOf(itemDef.code);
    const teamRareDex = teamRareLeft.indexOf(itemDef.code);
    if (teamRareDex >= 0) {
      teamRareLeft.splice(teamRareDex, 1);
    } else if (startIdx >= 0) { // 1. it was in starting inventory
      startingInventoryLeft.splice(startIdx, 1);
      const found = startingInventoryNeeded.find(
        (it) => it.code === itemDef.code && it.amount < it.maxStacks,
      );
      if (found == null) {
        startingInventoryNeeded.push({
          code: itemDef.code,
          amount: 1,
          maxStacks: itemDef.stackable,
          itemType: itemDef.itemType,
          weaponType: itemDef.weaponType,
        });
      } else {
        found.amount += 1;
      }
    } else if (invIdx >= 0) { // 2. it was excess from another multi-yield build
      tempInventory.splice(invIdx, 1);
      const find = combinesNeeded.find(
        (it) => it.code === itemDef.code && it.amountProduced < itemDef.initialCount,
      );
      if (find != null) {
        find.amountProduced += 1;
      } else {
        throw Error('illegal state');
      }
      // 3. it is a basic component and can be gotten immediately
    } else if (itemDef.makeMaterial2 === 0) {
      // can just do nothing here (mostly)
      const found = bComponentsNeeded.find(
        (it) => it.code === itemDef.code && it.amount < it.maxStacks,
      );
      if (found == null) {
        bComponentsNeeded.push({
          amount: 1,
          maxStacks: itemDef.stackable,
          code: itemDef.code,
          itemType: itemDef.itemType,
          weaponType: itemDef.weaponType,
        });
      } else {
        found.amount += 1;
      }
    } else { // 4. it is complex and must be built
      // add the excess to temp inventory
      for (let i = 1; i < itemDef.initialCount; i += 1) {
        tempInventory.push(itemDef.code);
      }
      needToBuild.push(itemCodeToDef[itemDef.makeMaterial1]);
      needToBuild.push(itemCodeToDef[itemDef.makeMaterial2]);
      combinesNeeded.push(
        {
          amountProduced: 1,
          code: itemDef.code,
          material1: itemDef.makeMaterial1,
          material2: itemDef.makeMaterial2,
        },
      );
    }
  }
  return {
    basicComponentsNeeded: bComponentsNeeded,
    combinesNeeded,
    startingInventoryNeeded,
  };
}

export interface HasInventory {
  inventory: InventoryState,
  basicComponentsTaken: Record<number, number>,
}

export interface ComponentsNeeded {
  basicComponentsNeeded: InventoryStack[],
}

export interface AdvancedRoutingState extends
  RoutingState<AdvancedRoutingState>, HasInventory, ComponentsNeeded {
  basicComponentsNeeded: InventoryStack[],
  combinesRemaining: CombineRule[],
  inventory: InventoryState,
  basicComponentsTaken: Record<number, number>,
}

export interface AdvancedRoutingConfig {
  inventoryProtection: boolean, // rejects if possible to run out of space
  inventorySize: number, // default should be 10
  zoneConstraints?: (null|(number[]))[],
  maxZones?: number,
  returnIfMax?: boolean, // used here for inventory simulation UI stuff.
  allZonesHaveTP?: boolean,
  minLeatherZone?: number|undefined,
  enableLootDistConstraint?: boolean|undefined,
  ignoreNoValueZones: boolean,
  teamRareZone: number,
  teamRareItems: number[],
  // Not actually used for normal routing.
}

export const defaultAdvancedRoutingConfig: AdvancedRoutingConfig = {
  inventoryProtection: true,
  zoneConstraints: [],
  inventorySize: 10,
  maxZones: 6,
  returnIfMax: false,
  allZonesHaveTP: false,
  minLeatherZone: 2,
  enableLootDistConstraint: true,
  ignoreNoValueZones: true,
  teamRareZone: 3,
  teamRareItems: [],
};

function copyInventory(
  previousInventory: InventoryState|undefined, startingInventory: InventoryStack[],
): InventoryStack[] {
  if (previousInventory != null) {
    return previousInventory.inventory.concat(
      ...(([
        previousInventory.weapon,
        previousInventory.chest,
        previousInventory.head,
        previousInventory.arm,
        previousInventory.leg,
        previousInventory.trinket,
      ]
        .filter((it) => it != null) as number[])
        .map((it) => {
          const itemCodeToDefElement = itemCodeToDef[it];
          return (
            {
              code: it,
              amount: 1,
              maxStacks: 1,
              itemType: itemCodeToDefElement.itemType,
              weaponType: itemCodeToDefElement.weaponType,
            } as InventoryStack
          );
        })),
    );
  }
  return _.cloneDeep(startingInventory);
}

function compressToInventoryState(
  inventory: InventoryStack[],
  weaponTypes: string[],
  targetItems: number[],
): InventoryState {
  const newInventory: InventoryStack[] = [];
  let weapon: null|number = null;
  let arm: null|number = null;
  let head: null|number = null;
  let chest: null|number = null;
  let trinket: null|number = null;
  let leg: null|number = null;
  // this sorting makes inventory sim look better in the UI
  // putting target items first means they will be prioritized for inventory slots.
  inventory.sort((a, b) => {
    const aInTarget = targetItems.includes(a.code) ? 0 : 1;
    const bInTarget = targetItems.includes(b.code) ? 0 : 1;
    return aInTarget - bInTarget;
  });
  inventory.forEach((it) => {
    const itemDef = itemCodeToDef[it.code];
    if (weapon == null && itemDef.weaponType != null && weaponTypes.includes(itemDef.weaponType)) {
      weapon = it.code;
    } else if (arm == null && itemDef.armorType === ApiArmorType.Arm) {
      arm = it.code;
    } else if (head == null && itemDef.armorType === ApiArmorType.Head) {
      head = it.code;
    } else if (chest == null && itemDef.armorType === ApiArmorType.Chest) {
      chest = it.code;
    } else if (trinket == null && itemDef.armorType === ApiArmorType.Trinket) {
      trinket = it.code;
    } else if (leg == null && itemDef.armorType === ApiArmorType.Leg) {
      leg = it.code;
    } else {
      const stackIndex = newInventory.findIndex(
        (nextItem) => nextItem.code === it.code && nextItem.amount < nextItem.maxStacks,
      );
      if (stackIndex >= 0) {
        const theStack = newInventory[stackIndex];
        const amountToAdd = Math.min(it.amount, theStack.maxStacks - theStack.amount);
        theStack.amount += amountToAdd;
        if (amountToAdd < it.amount) {
          newInventory.push({ ...it, amount: (it.amount - amountToAdd) });
        }
      } else {
        newInventory.push(it);
      }
    }
  });
  return {
    inventory: newInventory,
    weapon,
    arm,
    head,
    chest,
    trinket,
    leg,
  } as InventoryState;
}

export interface AdvancedRoutingResult {
  states: AdvancedRoutingState[],
  zones: number[],
}

export function advancedMultiItemRouting(
  targetItemCodes: number[],
  startingItems: number[],
  numToReturn: number,
  config: AdvancedRoutingConfig,
  weaponTypes: string[],
): AdvancedRoutingResult[] {
  const requirements = computeNeededComponentsAndCombines(
    targetItemCodes,
    startingItems,
    config.teamRareItems,
  );
  const epsilon = 0.001;
  const totalCombines = requirements.combinesNeeded.length;
  const combinesDiv = totalCombines + epsilon;
  const incompleteCostMultiplier = 2.0;
  const weaponNotDoneCost = 4;
  const minLeatherZoneNum = 2; // todo: compute this by examining weapons in targets.
  //                                    Would be when highest level of weapon craft-able
  //                                    w/o leather is built. May be issues for zone item
  //                                    is built in
  const leatherCode = 401103;
  // const firstItemCode = targetItemCodes[0];
  // const weaponNeedsLeather = itemCodeToDef[firstItemCode].weaponType != null
  //   && basicComponentsNeeded[firstItemCode].includes(leatherCode);

  const maxZones = config.maxZones || 6;
  const result = aStartSearch(
    (previousState, zoneId) => {
      if (config.zoneConstraints != null) {
        const idx = previousState?.numZones || 0;
        if (config.zoneConstraints.length > idx
          && config.zoneConstraints[idx] != null && !config.zoneConstraints[idx]!!.includes(zoneId)
        ) {
          return [null, 0];
        }
      }
      let combinesLeft = (
        previousState?.combinesRemaining || requirements.combinesNeeded
      );
      const currentInventory = copyInventory(
        previousState?.inventory, requirements.startingInventoryNeeded,
      );
      // step 1. get zone items
      const startingCombines = combinesLeft.length;
      const itemsInZone = zoneItems[zoneId];
      const zoneNum = ((previousState?.numZones || 0) + 1);
      const stacksTaken: Record<number, number> = {};
      const currentZoneExpectedStacks = zoneExpectedItemCounts[zoneId];
      const basicComponentsLeft = (
        previousState?.basicComponentsNeeded || requirements.basicComponentsNeeded
      ).map((it) => {
        const alreadyTakenAmount = stacksTaken[it.code] || 0;
        if (itemsInZone.has(it.code) && (
          it.code !== leatherCode || zoneNum >= minLeatherZoneNum)
        ) {
          if (config.enableLootDistConstraint && currentZoneExpectedStacks[it.code] != null) {
            const amountCanGet = Math.max(
              0,
              Math.min(it.amount, currentZoneExpectedStacks[it.code] - alreadyTakenAmount),
            );
            if (amountCanGet > 0) {
              stacksTaken[it.code] = alreadyTakenAmount + amountCanGet;
              if (amountCanGet === it.amount) {
                currentInventory.push(it);
                return null;
              }
              currentInventory.push(
                {
                  amount: amountCanGet,
                  code: it.code,
                  itemType: it.itemType,
                  maxStacks: it.maxStacks,
                  weaponType: it.weaponType,
                },
              );
              return {
                amount: it.amount - amountCanGet,
                code: it.code,
                itemType: it.itemType,
                maxStacks: it.maxStacks,
                weaponType: it.weaponType,
              };
            }
            return it;
          }
          stacksTaken[it.code] = alreadyTakenAmount + it.amount;
          currentInventory.push(it);
          return null;
        }
        return it;
      }).filter((it) => it != null);
      if ((previousState?.numZones || 0) + 1 === config.teamRareZone) {
        config.teamRareItems.forEach((it) => {
          stacksTaken[it] = (stacksTaken[it] || 0) + 1;
        });
        currentInventory.push(...config.teamRareItems.map((it) => makeInventoryStack(it, 1)));
      }
      if (config.ignoreNoValueZones && Object.keys(stacksTaken).length === 0) {
        return [null, 0];
      }
      // in future stuff, want to use stacks with lower amounts first to free up space.
      currentInventory.sort((a, b) => a.amount - b.amount);
      // step 2. do combines facilitated by zone until no combines are performed.
      let didCombine = true;
      while (didCombine) {
        let localDidCombine = false;
        combinesLeft = combinesLeft.filter((rule) => {
          const leftInventoryIdx = currentInventory.findIndex((it) => it.code === rule.material1);
          const rightInventoryIdx = currentInventory.findIndex((it) => it.code === rule.material2);
          // cases:
          // 1. both from inventory
          if (leftInventoryIdx >= 0 && rightInventoryIdx >= 0) {
            localDidCombine = true;
            const itemDef = itemCodeToDef[rule.code];
            currentInventory.push({
              code: rule.code,
              amount: rule.amountProduced,
              itemType: itemDef.itemType,
              weaponType: itemDef.weaponType,
              maxStacks: itemDef.stackable,
            });
            // delete rightmost first so that when leftmost is deleted things aren't shifted first.
            const maxIdx = Math.max(leftInventoryIdx, rightInventoryIdx);
            if (currentInventory[maxIdx].amount <= 1) {
              currentInventory.splice(maxIdx, 1);
            } else {
              const inventoryStack = currentInventory[maxIdx];
              currentInventory[maxIdx] = { ...inventoryStack, amount: inventoryStack.amount - 1 };
            }
            const minIdx = Math.min(leftInventoryIdx, rightInventoryIdx);
            if (currentInventory[minIdx].amount <= 1) {
              currentInventory.splice(minIdx, 1);
            } else {
              const inventoryStack = currentInventory[minIdx];
              currentInventory[minIdx] = { ...inventoryStack, amount: inventoryStack.amount - 1 };
            }
            return false;
          }
          return true;
        });
        didCombine = localDidCombine;
      }
      const newInventoryState = compressToInventoryState(
        currentInventory, weaponTypes, targetItemCodes,
      );
      if (
        config.inventoryProtection
        && newInventoryState.inventory.filter(
          (it) => !universalCollectables.includes(it.code),
        ).length > config.inventorySize
      ) {
        return [null, 0];
      }
      const weaponCost = newInventoryState.weapon == null
      || !targetItemCodes.includes(newInventoryState.weapon)
        ? weaponNotDoneCost
        : 0.0;
      return [
        {
          parent: previousState,
          numZones: zoneNum,
          combinesRemaining: combinesLeft,
          basicComponentsNeeded: _.cloneDeep(basicComponentsLeft),
          inventory: newInventoryState,
          zone: zoneId,
          basicComponentsTaken: stacksTaken,
        } as AdvancedRoutingState,
        (((startingCombines - combinesLeft.length) / combinesDiv)
        + (combinesLeft.length / combinesDiv) * incompleteCostMultiplier) * zoneNum + weaponCost,
      ];
    },
    (state) => {
      const shortCircuit = config.returnIfMax === true && state.numZones >= maxZones;
      // console.log('testing route with', state.numZones, 'zones and got', shortCircuit);
      return state.basicComponentsNeeded.length === 0 || shortCircuit;
    },
    numToReturn,
    (state) => (state.combinesRemaining.length / combinesDiv) * state.numZones,
    maxZones,
    10,
    5,
    (state) => zoneNumsToKey(toZoneNumbersFromState(state), false),
    config.allZonesHaveTP,
  ) as ComparablePair<AdvancedRoutingState>[];
  return result.map(
    (it) => ({ states: toStateArray(it), zones: toZoneNumbers(it) } as
      AdvancedRoutingResult
    ),
  );
}
