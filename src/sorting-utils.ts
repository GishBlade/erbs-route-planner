import {
  ApiItem,
  baseRngComponents,
  completedItems,
  itemGradeProperties,
} from './erbs-data/ERBSJsonInterfaces';

export default function sortItemList<T>(
  list: T[],
  toItemDef: (T) => ApiItem,
  rngComponentsGooder = false,
  goldIsGooder = false,
  completeIsGooder = false,
) {
  list.sort((a, b) => {
    const aDef = toItemDef(a);
    const bDef = toItemDef(b);
    if (rngComponentsGooder) {
      const aRngComponent = baseRngComponents.includes(aDef.code) ? 0 : 1;
      const bRngComponent = baseRngComponents.includes(bDef.code) ? 0 : 1;
      if (aRngComponent !== bRngComponent) {
        return aRngComponent - bRngComponent;
      }
    }
    if (goldIsGooder) {
      const aRarity = -itemGradeProperties[aDef.itemGrade].sortingPriority;
      const bRarity = -itemGradeProperties[bDef.itemGrade].sortingPriority;
      if (aRarity !== bRarity) {
        return aRarity - bRarity;
      }
    }
    if (completeIsGooder) {
      const aComplete = completedItems.has(aDef.code) ? 0 : 1;
      const bComplete = completedItems.has(bDef.code) ? 0 : 1;
      if (aComplete !== bComplete) {
        return aComplete - bComplete;
      }
    }
    return 0;
  });
}
