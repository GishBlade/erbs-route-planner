import React, {
  CSSProperties, useEffect, useRef, useState,
} from 'react';
import _ from 'lodash';
import { Canvas } from './Canvas';
import { zoneIdToName, zoneLookupTable } from '../erbs-data/ERBSJsonInterfaces';
import { characterColors } from '../erbs-data/MiscInfo';

type LumiaIslandAreaConsumer = (area: string, index: number, event: Event) => void;

export interface LumiaIslandMapProps {
  // image info
  refName: string
  // width: number
  // height: number
  // callbacks
  onClick?: LumiaIslandAreaConsumer|null
  onMouseMove?: LumiaIslandAreaConsumer|null
  onImageClick?: LumiaIslandAreaConsumer|null
  onLoad?: ((canvas: HTMLCanvasElement) => void)|null
  postLoad?: ((canvas: HTMLCanvasElement) => void)|null
  onMouseEnter?: LumiaIslandAreaConsumer|null
  onMouseLeave?: LumiaIslandAreaConsumer|null
  maxWidth?: number|null,
  activeCharacter: number,
  selectedZoneIds: number[][]
  setSelectedZoneIds: (arr: number[][]) => void
}

export interface Zone {
  name: string,
  coords: number[],
  centerX: number,
  centerY: number,
}

// todo: add zone ids to these definitions
const rawCoordinates: Zone[] = [
  {
    name: 'Archery Range',
    centerX: 272,
    centerY: 293,
    coords: [
      203, 348,
      221, 330,
      222, 320,
      205, 302,
      240, 265,
      235, 261,
      265, 233,
      287, 254,
      326, 216,
      318, 209,
      319, 205,
      323, 202, // Alley leftmost point
      375, 256, // Alley -> School border
      326, 306,
      345, 326,
      271, 400, // School -> Hotel Border
      242, 370,
      233, 378,
      203, 348,
    ],
  },
  {
    name: 'School',
    centerX: 370,
    centerY: 380,
    coords: [ // this one going CCW instead of CW
      375, 256, // Alley -> School border
      326, 306,
      345, 326,
      271, 400, // School -> Hotel Border
      358, 486, // Hotel/Forest School
      481, 362, // Avenue corner
      375, 256,
    ],
  },
  {
    name: 'Alley',
    centerX: 450,
    centerY: 180,
    coords: [
      324, 203,
      445, 77,
      475, 105,
      501, 81,
      540, 120,
      557, 107,
      648, 197,
      628, 216,
      645, 234,
      624, 255,
      590, 222,
      586, 225,
      563, 203,
      441, 319,
      324, 203,
    ],
  },
  {
    name: 'Avenue',
    centerX: 557,
    centerY: 315,
    coords: [
      444, 322,
      564, 208,
      586, 228,
      590, 226,
      672, 308,
      651, 330,
      677, 357,
      622, 410,
      610, 399,
      578, 431,
      582, 434,
      529, 489,
      489, 451,
      491, 448,
      470, 427,
      451, 451,
      439, 463,
      412, 436,
      484, 363,
      444, 322,
    ],
  },
  {
    name: 'Temple',
    centerX: 743,
    centerY: 315,
    coords: [
      627, 258,
      660, 227,
      682, 247,
      705, 225,
      730, 250,
      746, 235,
      834, 324,
      825, 334,
      858, 368,
      789, 434,
      718, 366,
      704, 379,
      655, 330,
      676, 308,
      627, 258,
    ],
  },
  {
    name: 'Pond',
    centerX: 670,
    centerY: 462,
    coords: [
      531, 491,
      584, 436,
      582, 431,
      610, 403,
      622, 411,
      679, 360,
      704, 383,
      718, 369,
      805, 453,
      727, 531,
      736, 542,
      661, 617,
      622, 579,
      597, 568,
      560, 531,
      566, 522,
      546, 502,
      543, 503,
      531, 491,
    ],
  },
  {
    name: 'Hotel',
    centerX: 231,
    centerY: 482,
    coords: [
      203, 351,
      234, 380,
      241, 374,
      356, 487,
      257, 586,
      228, 559,
      223, 563,
      209, 550,
      197, 561,
      153, 518,
      159, 513,
      114, 468,
      119, 461,
      109, 451,
      163, 398,
      161, 394,
      203, 351,
    ],
  },
  {
    name: 'Beach',
    centerX: 222,
    centerY: 640,
    coords: [
      156, 525,
      197, 565,
      208, 557,
      221, 569,
      229, 563,
      257, 590,
      271, 579,
      326, 633,
      241, 721,
      167, 662,
      139, 563,
      143, 545,
      156, 527,
    ],
  },
  {
    name: 'Forest',
    centerX: 391,
    centerY: 622,
    coords: [
      411, 441,
      437, 465,
      374, 523,
      501, 639,
      419, 720,
      275, 573,
      411, 441,
    ],
  },
  {
    name: 'Cemetery',
    centerX: 585,
    centerY: 630,
    coords: [
      558, 534,
      594, 570,
      622, 581,
      662, 620,
      671, 614,
      734, 677,
      657, 754,
      591, 688,
      573, 706,
      479, 613,
      490, 596,
      558, 534,
    ],
  },
  {
    name: 'Hospital',
    centerX: 778,
    centerY: 579,
    coords: [
      808, 457,
      818, 465,
      799, 486,
      874, 562,
      764, 671,
      753, 660,
      737, 674,
      673, 612,
      740, 543,
      731, 531,
      808, 457,
    ],
  },
  {
    name: 'Uptown',
    centerX: 342,
    centerY: 762,
    coords: [
      331, 636,
      427, 733,
      465, 749,
      477, 762,
      378, 860,
      328, 823,
      319, 831,
      295, 809,
      285, 818,
      263, 796,
      270, 789,
      253, 767,
      269, 749,
      244, 723,
      331, 636,
    ],
  },
  {
    name: 'Chapel',
    centerX: 546,
    centerY: 752,
    coords: [
      503, 641,
      573, 708,
      591, 693,
      655, 756,
      566, 845,
      468, 748,
      429, 731,
      422, 723,
      503, 641,
    ],
  },
  {
    name: 'Factory',
    centerX: 729,
    centerY: 792,
    coords: [
      754, 665,
      793, 700,
      787, 708,
      856, 778,
      794, 840,
      777, 825,
      722, 879,
      713, 872,
      694, 890,
      649, 847,
      642, 853,
      602, 814,
      754, 665,
    ],
  },
  {
    name: 'Dock',
    centerX: 491,
    centerY: 870,
    coords: [
      480, 765,
      566, 849,
      600, 816,
      640, 855,
      564, 931,
      554, 922,
      518, 958,
      471, 913,
      450, 933,
      403, 888,
      381, 863,
      480, 765,
    ],
  },
];

function numsToZoneDef(nums: number[], coordinates: Zone[]): Zone[] {
  return nums.map((it) => {
    const name = zoneIdToName[it];
    const result = coordinates.find((z) => z.name === name);
    if (result) {
      return result;
    }
    throw Error();
  });
}

interface CoordinateMappingInfo {
  offset: number,
  scale: number,
}

const noScaling: CoordinateMappingInfo = {
  offset: 0,
  scale: 1.0,
};

const baseXOffset: CoordinateMappingInfo = { offset: 102, scale: 1.0 };
const baseYOffset: CoordinateMappingInfo = { offset: 74, scale: 1.0 };

function scaleCoordinate(coord: number, multiplier: number, info: CoordinateMappingInfo) {
  return Math.floor(multiplier * (coord - info.offset) * info.scale);
}

export function LumiaIslandMap({
  // refName,
  selectedZoneIds,
  setSelectedZoneIds,
  maxWidth = null,
  activeCharacter,
  // postLoad = null,
  // onLoad = null,
  // onClick = null,
  // onMouseMove = null,
  // onImageClick = null,
  // onMouseEnter = null,
  // onMouseLeave = null,
}: LumiaIslandMapProps) {
  const src = '/BlankMap-cropped.webp';
  const imageRef = useRef(null);
  const [hoveredZone, setHoveredZone] = useState(null as Zone|null);
  const [coordinates, setCoordinates] = useState(rawCoordinates);
  // const [selectedZones, setSelectedZones] = useState([[]] as Zone[][]);
  const maxXDim = 778;
  const multiplier = (maxWidth || maxXDim) / maxXDim;
  const maxYDim = 892;
  const width = scaleCoordinate(maxXDim, multiplier, noScaling);
  const height = scaleCoordinate(maxYDim, multiplier, noScaling);
  useEffect(() => {
    setCoordinates(
      rawCoordinates.map((zone) => {
        const newZone: Zone = {
          name: zone.name,
          centerX: scaleCoordinate(zone.centerX, multiplier, baseXOffset),
          centerY: scaleCoordinate(zone.centerY, multiplier, baseYOffset),
          coords: zone.coords.map(
            (it, idx) => scaleCoordinate(it, multiplier, idx % 2 === 0 ? baseXOffset : baseYOffset),
          ),
        };
        return newZone;
      }),
    );
  }, [multiplier]);

  const selectedZones = selectedZoneIds.map((it) => numsToZoneDef(it, coordinates));

  function inRoute(zone: Zone, player: number = 0): boolean {
    return selectedZones[player].find((z) => z.name === zone.name) != null;
  }

  function addZone(zone: Zone, player) {
    if (!inRoute(zone, player)) {
      const cloneDeep = _.cloneDeep(selectedZoneIds);
      cloneDeep[player].push(zoneLookupTable[zone.name]);
      setSelectedZoneIds(cloneDeep);
    }
  }

  function removeZone(zone: Zone, player) {
    if (inRoute(zone, player)) {
      const cloneDeep = _.cloneDeep(selectedZoneIds);
      const zonesId = zoneLookupTable[zone.name];
      cloneDeep[player] = cloneDeep[player].filter((z) => z !== zonesId);
      setSelectedZoneIds(cloneDeep);
    }
  }

  function resolveZoneClick(zone: Zone, player) {
    if (inRoute(zone, player)) {
      removeZone(zone, player);
    } else {
      addZone(zone, player);
    }
  }
  const hoverFillColor = '#3440eb55';

  const absPos: CSSProperties = { position: 'absolute', top: 0, left: 0 };

  return (
    <div style={{ width, height, margin: 10 }}>
      <div style={{ position: 'relative', width: '100%', height: '100%' }}>
        <img
          src={src}
          alt=""
          ref={imageRef}
          style={{ width: '100%', height: '100%', ...absPos }}
        />
        {/* <canvas ref={imageRef} width={width} height={height} /> */}
        <Canvas
          style={{ zIndex: 1, ...absPos }}
          drawFn={(context, canvas) => {
            context.clearRect(0, 0, canvas.width, canvas.height);
            if (hoveredZone != null) {
              context.beginPath();
              context.moveTo(hoveredZone.coords[0], hoveredZone.coords[1]);
              for (let x = 2; x < hoveredZone.coords.length; x += 2) {
                context.lineTo(hoveredZone.coords[x], hoveredZone.coords[x + 1]);
              }
              // hoveredZone.coords[]
              context.fillStyle = hoverFillColor;
              context.fill();
              context.closePath();
            }
            const numPlayers = selectedZones.length;
            const boxSize = scaleCoordinate(36, multiplier, noScaling);
            const fullWidth = boxSize * numPlayers;
            const startXOffset = -(fullWidth / 2);
            const yOffset = -(boxSize / 2);
            const playerParams = selectedZones.map((zoneArr, playerNum) => {
              const playerXOffset = startXOffset + (playerNum * boxSize);
              const playerFillColor = characterColors[playerNum];
              const routeCoordinates = zoneArr.map((zone) => {
                const x = Math.floor(zone.centerX + playerXOffset);
                const y = Math.floor(zone.centerY + yOffset);
                return { x, y };
              });
              return {
                playerXOffset,
                playerFillColor,
                routeCoordinates,
              };
            });
            // do lines first to avoid over-writing stuff
            selectedZones.forEach((zoneArr, playerNum) => {
              const { playerFillColor, routeCoordinates } = playerParams[playerNum];
              for (let i = 1; i < routeCoordinates.length; i += 1) {
                context.strokeStyle = playerFillColor;
                context.lineWidth = 5;
                const startCoord = routeCoordinates[i - 1];
                const endCoord = routeCoordinates[i];
                context.beginPath();
                const offset = Math.floor(boxSize / 2);
                context.moveTo(startCoord.x + offset, startCoord.y + offset);
                context.lineTo(endCoord.x + offset, endCoord.y + offset);
                context.stroke();
              }
            });
            selectedZones.forEach((zoneArr, playerNum) => {
              const { playerFillColor, routeCoordinates } = playerParams[playerNum];
              routeCoordinates.forEach((c, numInZone) => {
                context.fillStyle = playerFillColor;
                context.fillRect(
                  c.x,
                  c.y,
                  boxSize,
                  boxSize,
                );
                context.fillStyle = 'white';
                context.font = `${scaleCoordinate(boxSize, multiplier, noScaling)}px Arial`;
                context.fillText(`${numInZone + 1}`, c.x + boxSize / 4, c.y + ((boxSize / 4) * 3));
              });
            });
          }}
          width={width}
          height={height}
        />
        <img
          src={src}
          useMap="#lumia-map"
          alt=""
          ref={imageRef}
          style={{
            width: '100%', height: '100%', ...absPos, zIndex: 2, opacity: 0,
          }}
        />
        <map name="lumia-map" style={{ zIndex: 2 }}>
          {coordinates.map((zone) => (
            <area
              key={`lumia-island-map-${zone.name}`}
              aria-hidden
              alt={`area-${zone.name}`}
              shape="poly"
              coords={zone.coords.join(',')}
              onMouseEnter={() => {
                setHoveredZone(zone);
              }}
              onMouseLeave={() => {
                if (zone === hoveredZone) {
                  setHoveredZone(null);
                }
              }}
              onClick={() => {
                resolveZoneClick(zone, activeCharacter);
              }}
              onKeyPress={() => {}}
            />
          ))}
        </map>
      </div>
    </div>
  );
}
