import React, { useEffect, useRef } from 'react';

export interface CanvasProps {
  drawFn: (CanvasRenderingContext2D, HTMLCanvasElement) => void
  [x: string]: any
}

export function Canvas(props: CanvasProps) {
  const { drawFn, ...rest } = props;
  const canvasRef = useRef<HTMLCanvasElement>(null);

  useEffect(() => {
    const canvas = canvasRef.current;
    if (canvas != null) {
      const context = canvas.getContext('2d');
      let animationFrameId;
      const render = () => {
        drawFn(context, canvas);
        animationFrameId = window.requestAnimationFrame(render);
      };
      render();
      return window.cancelAnimationFrame(animationFrameId);
    }
    return () => {};
  }, [drawFn]);
  return <canvas ref={canvasRef} {...rest} />;
}
