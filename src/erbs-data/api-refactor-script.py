#!/usr/bin/env python3
import json
from pathlib import Path
from typing import List

property_names = [
    "influencePoint",
    "attackPower",
    "attackPowerByLv",
    "attackRange",
    "attackSpeedRatio",
    "attackSpeedRatioByLv",
    "cooldownLimit",
    "cooldownReduction",
    "criticalStrikeChance",
    "criticalStrikeDamage",
    "defense",
    "defenseByLv",
    # "exclusiveProducer",
    "fullDecreaseRecoveryToBasicAttackMelee",
    "fullDecreaseRecoveryToBasicAttackRange",
    "fullDecreaseRecoveryToSkillMelee",
    "fullDecreaseRecoveryToSkillRange",
    "halfDecreaseRecoveryToBasicAttackMelee",
    "halfDecreaseRecoveryToBasicAttackRange",
    "halfDecreaseRecoveryToSkillMelee",
    "halfDecreaseRecoveryToSkillRange",
    "hpRegen",
    "hpRegenRatio",
    "increaseBasicAttackDamage",
    "increaseBasicAttackDamageByLv",
    "increaseBasicAttackDamageRatioByLv",
    "increaseSkillDamage",
    "increaseSkillDamageByLv",
    "increaseSkillDamageRatio",
    "increaseSkillDamageRatioByLv",
    # "initialCount",
    "lifeSteal",
    "maxHp",
    "maxHpByLv",
    "maxSp",
    "moveSpeed",
    "outOfCombatMoveSpeed",
    "penetrationDefense",
    "penetrationDefenseRatio",
    "preventBasicAttackDamaged",
    "preventBasicAttackDamagedByLv",
    "preventBasicAttackDamagedRatioByLv",
    "preventCriticalStrikeDamaged",
    "preventSkillDamaged",
    "preventSkillDamagedByLv",
    "preventSkillDamagedRatio",
    "preventSkillDamagedRatioByLv",
    "sightRange",
    "spRegen",
    "spRegenRatio",
    "trapDamageReduce",
    "trapDamageReduceRatio",
    "attackSpeed",
    "attackSpeedLimit",
    "attackSpeedMin",
    "attack",
    "assistance",
    "controlDifficulty",
    "disruptor",
    "move",
    "spRecover",
    "heal",
    "hpRecover",
    "defenseByBuff",
    "attackPowerByBuff",
    "healerGiveHpHealRatio",
    "hpHealedIncreaseRatio",
    # "moveSpeedOutOfCombat", changed to be consistent with armor
    "normalLifeSteal",
    "skillLifeSteal",
    "uniqueAttackRange",
    "uniqueCooldownLimit",
    "uniqueHpHealedIncreaseRatio",
    "uniqueTenacity",
    "uniqueLifeSteal",
    "uniquePenetrationDefense",
    "uniquePenetrationDefenseRatio",
    "uniqueMoveSpeed",
    "skillAmp",
    "skillAmpByLevel",
    "skillAmpRatio",
    "skillAmpRatioByLevel",
    "uniqueSkillAmpRatio",
]

mapToInt = [
    "itemCode",
    "code",
    "makeMaterial1",
    "makeMaterial2",
]

keysToFix = {
    "moveSpeedOutOfCombat": "outOfCombatMoveSpeed",
    "maxSP": "maxSp",
}

valuesToFix = {"weaponType": ("TwohandSword", "TwoHandSword")}

non_property_keys = set()


def rectify_object(original: dict) -> dict:
    fixed = {}
    for key, value in original.items():
        if key in keysToFix:
            key = keysToFix[key]
        if key in valuesToFix and value == valuesToFix[key][0]:
            value = valuesToFix[key][1]
        if key in mapToInt and isinstance(value, str):
            value = int(value)
        if key in property_names:
            # if "properties" not in fixed:
            #     fixed["properties"] = []
            # fixed["properties"].append({"name": key, "value": value})
            if "properties" not in fixed:
                fixed["properties"] = {}
            fixed["properties"][key] = value
        else:
            non_property_keys.add(key)
            fixed[key] = value
    return fixed


for file in Path("./api-original-data").iterdir():
    if file.is_dir() or not file.name.endswith(".json"):
        continue
    with open(file, "r") as input_file:
        loaded_json: List[dict] = json.load(input_file)

    output_path = Path("./") / "api-refactored-data" / file.name
    with open(output_path, "w") as output_file:
        json.dump(
            list(map(rectify_object, loaded_json)),
            output_file,
            indent=2,
            sort_keys=True,
        )

# print("non-property-set", non_property_keys)
