import { defaultStartingItems, rngItems } from './ERBSJsonInterfaces';

export const singleRouteStartingItems = defaultStartingItems.concat(rngItems) as number[];

export const characterColors = [
  '#1DACFF',
  '#9E0C00',
  '#D0C300',
];
