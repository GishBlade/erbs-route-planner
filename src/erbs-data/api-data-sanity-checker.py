#!/usr/bin/env python3
import json
from typing import List, Optional, Dict, Tuple

from pydantic import BaseModel
from enum import Enum


class WeaponType(str, Enum):
    AssaultRifle = "AssaultRifle"
    Axe = "Axe"
    Bat = "Bat"
    Bow = "Bow"
    Camera = "Camera"
    CrossBow = "CrossBow"
    OneHandSword = "OneHandSword"
    DualSword = "DualSword"
    Glove = "Glove"
    Guitar = "Guitar"
    Hammer = "Hammer"
    Nunchaku = "Nunchaku"
    Pistol = "Pistol"
    Rapier = "Rapier"
    DirectFire = "DirectFire"
    SniperRifle = "SniperRifle"
    Spear = "Spear"
    HighAngleFire = "HighAngleFire"
    Tonfa = "Tonfa"
    TwoHandSword = "TwoHandSword"
    Whip = "Whip"
    Arcana = "Arcana"
    VFArm = "VFArm"


class ApiArmorType(str, Enum):
    Arm = "Arm"
    Chest = "Chest"
    Head = "Head"
    Leg = "Leg"
    Trinket = "Trinket"


class ApiItemType(str, Enum):
    Armor = "Armor"
    Consume = "Consume"
    Misc = "Misc"
    Special = "Special"
    Weapon = "Weapon"
    DnaBracelet = "DnaBracelet"
    EscapeMaterial = "EscapeMaterial"
    EscapeKey = "EscapeKey"
    EscapeQualification = "EscapeQualification"


class ApiItemGrade(str, Enum):
    Common = "Common"
    Epic = "Epic"
    Legend = "Legend"
    Rare = "Rare"
    Uncommon = "Uncommon"
    Mythic = "Mythic"


class SpecialItemType(str, Enum):
    Summon = "Summon"


class ConsumableType(str, Enum):
    Beverage = "Beverage"
    Food = "Food"
    SpecialBeverage = "SpecialBeverage"
    SpecialFood = "SpecialFood"


class_filename_tuples: List[Tuple[any, str]] = []


class ApiArea(BaseModel):
    code: int
    maskCode: int
    name: str


class_filename_tuples.append((ApiArea, "Area.json"))


class ApiStartItem(BaseModel):
    code: int
    count: int
    groupCode: int
    itemCode: int
    modeType: int


class_filename_tuples.append((ApiStartItem, "StartItem.json"))


class ApiCollectable(BaseModel):
    code: int
    cooldown: int
    dropCount: int
    itemCode: str


class_filename_tuples.append((ApiCollectable, "Collectible.json"))


class ApiItem(BaseModel):
    itemGrade: ApiItemGrade
    itemType: ApiItemType
    code: int
    initialCount: int
    makeMaterial1: int  # seems to be 0 if base material, and item code otherwise
    makeMaterial2: int  # seems to be 0 if base material, and item code otherwise
    name: str
    stackable: int
    properties: Optional[Dict[str, int]]
    consumableType: Optional[ConsumableType]
    armorType: Optional[ApiArmorType]
    specialItemType: Optional[SpecialItemType]
    weaponType: Optional[WeaponType]


class_filename_tuples.append((ApiItem, "ItemArmor.json"))
class_filename_tuples.append((ApiItem, "ItemMisc.json"))
class_filename_tuples.append((ApiItem, "ItemWeapon.json"))


class ApiConsumable(ApiItem):
    consumableType: ConsumableType


class_filename_tuples.append((ApiConsumable, "ItemConsumable.json"))


class ApiItemSpecial(ApiItem):
    specialItemType: SpecialItemType  # is currently only "Summon"
    summonCode: int


class_filename_tuples.append((ApiItemSpecial, "ItemSpecial.json"))

base_model: BaseModel
for base_model, file_name in class_filename_tuples:
    with open(f"api-refactored-data/{file_name}", "r") as input_file:
        loaded: list = json.load(input_file)
    for thing in loaded:
        try:
            parsed = base_model(**thing)
        except Exception as e:
            print(e)
            print("ERROR while parsing for", file_name, "on datum", thing)
            exit(2)
