export enum WeaponType {
  AssaultRifle = 'AssaultRifle',
  Axe = 'Axe',
  Bat = 'Bat',
  Bow = 'Bow',
  Camera = 'Camera',
  CrossBow = 'CrossBow',
  OneHandSword = 'OneHandSword',
  DualSword = 'DualSword',
  Glove = 'Glove',
  Guitar = 'Guitar',
  Hammer = 'Hammer',
  Nunchaku = 'Nunchaku',
  Pistol = 'Pistol',
  Rapier = 'Rapier',
  DirectFire = 'DirectFire',
  SniperRifle = 'SniperRifle',
  Spear = 'Spear',
  HighAngleFire = 'HighAngleFire',
  Tonfa = 'Tonfa',
  TwoHandSword = 'TwoHandSword',
  Whip = 'Whip',
  Arcana = 'Arcana',
  VFArm = 'VFArm',
}

export const zoneLookupTable: Record<string, number> = {
  Dock: 1,
  Pond: 2,
  Beach: 3,
  Uptown: 4,
  Alley: 5,
  Hotel: 6,
  Avenue: 7,
  Hospital: 8,
  Temple: 9,
  'Archery Range': 10,
  Cemetery: 11,
  Forest: 12,
  Factory: 13,
  Chapel: 14,
  School: 15,
  'Research Center': 16,
};

export const zoneIdToName: Record<number, string> = {};
Object.entries(zoneLookupTable).forEach((entry) => {
  // eslint-disable-next-line prefer-destructuring
  zoneIdToName[entry[1]] = entry[0];
});

export const allZoneIds = Object.keys(zoneIdToName)
  .map((it) => parseInt(it, 10))
  .filter((it) => it < 16);

export interface WeaponTypeInfo {
  englishName: string,
  startingWeapon: number,
}

export const weaponInfo: Record<WeaponType, WeaponTypeInfo> = {
  Arcana: {
    englishName: 'Arcana',
    startingWeapon: 130101,
  },
  AssaultRifle: {
    englishName: 'Assault Rifle',
    startingWeapon: 117101,
  },
  Camera: {
    englishName: 'Camera',
    startingWeapon: 117101,
  },
  Axe: {
    englishName: 'Axe',
    startingWeapon: 105103,
  },
  Bat: {
    englishName: 'Bat',
    startingWeapon: 108102,
  },
  Bow: {
    englishName: 'Bow',
    startingWeapon: 114101,
  },
  CrossBow: {
    englishName: 'Crossbow',
    startingWeapon: 115101,
  },
  OneHandSword: {
    englishName: 'Dagger',
    startingWeapon: 101104,
  },
  DualSword: {
    englishName: 'Dual Swords',
    startingWeapon: 103201,
  },
  Glove: {
    englishName: 'Glove',
    startingWeapon: 110102,
  },
  Guitar: {
    englishName: 'Guitar',
    startingWeapon: 121101,
  },
  Hammer: {
    englishName: 'Hammer',
    startingWeapon: 104101,
  },
  Nunchaku: {
    englishName: 'Nunchaku',
    startingWeapon: 119101,
  },
  Pistol: {
    englishName: 'Pistol',
    startingWeapon: 116101,
  },
  Rapier: {
    englishName: 'Rapier',
    startingWeapon: 120101,
  },
  DirectFire: {
    englishName: 'Shuriken',
    startingWeapon: 113101,
  },
  SniperRifle: {
    englishName: 'Sniper Rifle',
    startingWeapon: 118101,
  },
  Spear: {
    englishName: 'Spear',
    startingWeapon: 107101,
  },
  HighAngleFire: {
    englishName: 'Throw',
    startingWeapon: 112105,
  },
  Tonfa: {
    englishName: 'Tonfa',
    startingWeapon: 108103,
  },
  TwoHandSword: {
    englishName: 'Two-handed Sword',
    startingWeapon: 102101,
  },
  VFArm: {
    englishName: 'VF Arm',
    startingWeapon: 131201, // assuming viper is the starting weapon
  },
  Whip: {
    englishName: 'Whip',
    startingWeapon: 109101,
  },
};

export interface ApiArea {
  code: number,
  maskCode: number,
  name: string,
}

export const apiAreas = require('./api-refactored-data/Area.json') as ApiArea[];

type ApiProperties = Record<string, number>;

// export interface ApiCharacter {
//   name: string,
//   code: number,
// }

export enum ConsumableType {
  Beverage = 'Beverage',
  Food = 'Food',
  SpecialBeverage = 'SpecialBeverage',
  SpecialFood = 'SpecialFood',
}

// export const apiCharacters = require('./api-refactored-data/Character.json') as ApiCharacter[];

// export interface ApiCharacterAttributes {
//   character: string,
//   characterCode: number,
//   mastery: WeaponType,
//   properties: ApiProperties,
// }
// export const apiCharacterAttributes =
// require('./api-refactored-data/CharacterAttributes.json') as ApiCharacterAttributes[];

export interface ApiStartItem extends ApiItem {
  code: number,
  count: number,
  groupCode: number,
  itemCode: number,
  modeType: number,
}
export const apiStartItems = require('./api-refactored-data/StartItem.json') as ApiStartItem[];

export const defaultStartingItems: Array<number> = [];

apiStartItems.forEach((startItem) => {
  // mode type is 7 for BR mode, and 8 for cobalt protocol
  if (startItem.modeType === 7) {
    for (let i = 0; i < startItem.count; i += 1) {
      defaultStartingItems.push(startItem.itemCode);
    }
  }
});

export interface ApiCollectable {
  code: number,
  cooldown: number,
  dropCount: number,
  itemCode: string,
}
export const apiCollectable = require('./api-refactored-data/Collectible.json') as ApiCollectable[];

export const universalCollectables = [
  108101, // branch
  112101, // stone
  // here to help solve build-ability issues....
  131101, // ": "Call of Cadmus Lv2",
  131102, // ": "Call of Cadmus Lv3",
  weaponInfo.VFArm.startingWeapon,
];

export interface ExtraZoneInfo {
  collectableItems: number[],
  animals: ('wolf'|'chicken'|'dog'|'bear'|'bat'|'boar')[],
}

const codCode = 301104;
const carpCode = 301109;
const potatoCode = 301102;
const waterCode = 302102;
const flowerCode = 205102;

export const extraZoneInfo: Record<string, ExtraZoneInfo> = {
  Dock: {
    collectableItems: universalCollectables.concat([
      codCode,
    ]),
    animals: ['dog', 'chicken', 'bear'],
  },
  Pond: {
    collectableItems: universalCollectables.concat([
      carpCode,
      waterCode,
      potatoCode,
      flowerCode,
    ]),
    animals: ['bear', 'boar', 'bat'],
  },
  Beach: {
    collectableItems: universalCollectables.concat([
      codCode,
    ]),
    animals: ['bear', 'bat', 'boar'],
  },
  Uptown: {
    collectableItems: universalCollectables.concat([
      codCode,
      flowerCode,
    ]),
    animals: ['chicken', 'bear', 'dog'],
  },
  Alley: {
    collectableItems: universalCollectables.concat([
      potatoCode,
    ]),
    animals: ['wolf', 'bear', 'chicken'],
  },
  Hotel: {
    collectableItems: universalCollectables.concat([
      waterCode,
    ]),
    animals: ['chicken', 'wolf', 'dog'],
  },
  Avenue: {
    collectableItems: universalCollectables.concat([]),
    animals: ['dog', 'boar', 'chicken'],
  },
  Hospital: {
    collectableItems: universalCollectables.concat([]),
    animals: ['wolf', 'dog', 'chicken'],
  },
  Temple: {
    collectableItems: universalCollectables.concat([
      potatoCode,
    ]),
    animals: ['bear', 'boar', 'bat'],
  },
  'Archery Range': {
    collectableItems: universalCollectables.concat([]),
    animals: ['wolf', 'bat', 'boar'],
  },
  Cemetery: {
    collectableItems: universalCollectables.concat([
      carpCode,
      waterCode,
      flowerCode,
    ]),
    animals: ['wolf', 'boar', 'bat'],
  },
  Forest: {
    collectableItems: universalCollectables.concat([
      carpCode,
      waterCode,
      flowerCode,
    ]),
    animals: ['wolf', 'bat', 'boar'],
  },
  Factory: {
    collectableItems: universalCollectables.concat([]),
    animals: ['wolf', 'dog', 'chicken'],
  },
  Chapel: {
    collectableItems: universalCollectables.concat([]),
    animals: ['boar', 'chicken', 'bat'],
  },
  School: {
    collectableItems: universalCollectables.concat([]),
    animals: ['dog', 'chicken', 'bat'],
  },
  'Research Center': {
    collectableItems: [],
    animals: [],
  },
};

// don't think I need this right now
// export interface ApiHowToFindItem {
//   airSupply: number,
//   code: number,
//   collectableCode: number,
//   huntBat: number
//   huntBear: number
//   huntBoar: number
//   huntChicken: number
//   huntWickline: number
//   huntWildDog: number
//   huntWolf: number
//   itemCode: number
// }

export enum ApiArmorType {
  Arm = 'Arm',
  Chest = 'Chest',
  Head = 'Head',
  Leg = 'Leg',
  Trinket = 'Trinket',
}

export enum ApiItemType {
  Armor = 'Armor',
  Consume = 'Consume',
  Misc = 'Misc',
  Special = 'Special',
  Weapon = 'Weapon',
  DnaBracelet = 'DnaBracelet',
  EscapeKey = 'EscapeKey',
  EscapeQualification = 'EscapeQualification',
}

export enum ApiItemGrade {
  Common = 'Common',
  Epic = 'Epic',
  Legend = 'Legend',
  Rare = 'Rare',
  Uncommon = 'Uncommon',
  Mythic = 'Mythic',
}

export interface ApiItemGradeProps {
  sortingPriority: number,
}

export const itemGradeProperties: Record<ApiItemGrade, ApiItemGradeProps> = {
  Common: {
    sortingPriority: 0,
  },
  Mythic: {
    sortingPriority: 5,
  },
  Epic: {
    sortingPriority: 3,
  },
  Rare: {
    sortingPriority: 2,
  },
  Legend: {
    sortingPriority: 4,
  },
  Uncommon: {
    sortingPriority: 1,
  },
};

export interface ApiItem {
  itemGrade: ApiItemGrade,
  itemType: ApiItemType,
  code: number,
  initialCount: number,
  makeMaterial1: number, // seems to be 0 if base material, and item code otherwise
  makeMaterial2: number, // seems to be 0 if base material, and item code otherwise
  name: string,
  stackable: number,
  properties: ApiProperties|null,
  consumableType: ConsumableType|null,
  armorType: ApiArmorType|null,
  specialItemType: SpecialItemType|null,
  weaponType: WeaponType|null,
}

export interface ApiItemArmor extends ApiItem {
  armorType: ApiArmorType
}
export const apiArmor = require('./api-refactored-data/ItemArmor.json') as ApiItemArmor[];

export interface ApiItemConsumable extends ApiItem {
  // consumableTag probably for rozzi/li-dailin stuff (values are None Alcohol and Chocolate
  consumableType: ConsumableType
}
export const apiConsumables = (require('./api-refactored-data/ItemConsumable.json') as ApiItemConsumable[]);

// enum MiscItemType {
//   Material,
//   None, // None is for the DNA bracelet so ignoring for now.
// }

export interface ApiItemMisc extends ApiItem {
  // currently, no additional properties past base item attributes
}
export const apiMiscItems = require('./api-refactored-data/ItemMisc.json') as ApiItemMisc[];

export interface ApiItemSpawn {
  areaCode: number,
  areaSpawnGroup: number,
  areaType: string,
  code: number,
  dropCount: number,
  itemCode: number,
  name: string,
  // dropPoint: string, // always "Fixed"
}
export const apiItemSpawns = require('./api-refactored-data/ItemSpawn.json') as ApiItemSpawn[];

export enum SpecialItemType {
  Summon = 'Summon',
}

export interface ApiItemSpecial extends ApiItem {
  // consumeCount: number, // value always 1
  specialItemType: SpecialItemType, // is currently only "Summon"
  summonCode: number,
}
export const apiItemSpecial = require('./api-refactored-data/ItemSpecial.json') as ApiItemSpecial[];

export interface ApiWeapon extends ApiItem {
  // consumeCount: number, // value always 1
  weaponType: WeaponType,
}
export const apiWeapon = require('./api-refactored-data/ItemWeapon.json') as ApiWeapon[];

const almostAllApiItems = (apiArmor as ApiItem[])
  .concat(apiConsumables)
  .concat(apiItemSpecial)
  .concat(apiMiscItems)
  .concat(apiWeapon);

// interface HowToFind {
//   itemCode: number,
// }
//
// const howToFind = require('./api-refactored-data/HowToFindItem.json') as HowToFind[];

export const allApiItems = almostAllApiItems;
export const allApiItemCodes: number[] = almostAllApiItems.map((it) => it.code);
// .concat(howToFind.filter((it) => !almostAllIds.includes(it.itemCode)).map((it) => ({
//   code: it.itemCode,
//   itemGrade: ApiItemGrade.Common,
//   itemType: ApiItemType.Misc,
//   initialCount: 1,
//   makeMaterial1: 0, // seems to be 0 if base material, and item code otherwise
//   makeMaterial2: 0, // seems to be 0 if base material, and item code otherwise
//   name: 'UNKNOWN',
//   stackable: 3,
//   properties: {},
//   consumableType: null,
//   armorType: null,
//   specialItemType: null,
//   weaponType: null,
// })));

const itemsUsedAsComponent = new Set<number>();
export const itemCodeToDef: Record<number, ApiItem> = {};
allApiItems.forEach((item) => {
  itemCodeToDef[item.code] = item;
});

export const zoneItems: Record<number, Set<number>> = {};
export const zoneExpectedItemCounts: Record<number, Record<number, number>> = {};
const allFindableItems = new Set<number>();

apiItemSpawns.forEach((spawnInfo) => {
  if (zoneItems[spawnInfo.areaCode] == null) {
    zoneItems[spawnInfo.areaCode] = new Set<number>();
  }
  zoneItems[spawnInfo.areaCode].add(spawnInfo.itemCode);
  if (spawnInfo.areaSpawnGroup !== 0) {
    if (zoneExpectedItemCounts[spawnInfo.areaCode] == null) {
      zoneExpectedItemCounts[spawnInfo.areaCode] = {};
    }
    zoneExpectedItemCounts[spawnInfo.areaCode][spawnInfo.itemCode] = spawnInfo.dropCount
      * itemCodeToDef[spawnInfo.itemCode].initialCount;
  }
  allFindableItems.add(spawnInfo.itemCode);
});
export const leatherCode = 401103;
allFindableItems.add(leatherCode);
defaultStartingItems.forEach((it) => allFindableItems.add(it));
universalCollectables.forEach((it) => allFindableItems.add(it));

Object.entries(extraZoneInfo).forEach(([zoneName, zoneInfo]) => {
  const zoneId = zoneLookupTable[zoneName];
  zoneInfo.collectableItems.forEach((itemCode) => {
    zoneItems[zoneId].add(itemCode);
    allFindableItems.add(itemCode);
  });
  // now to hard-code some leather zones
  // todo: make this adjustable
  if (zoneInfo.animals.includes('dog') || zoneInfo.animals.includes('wolf') || zoneInfo.animals.includes('chicken')) {
    zoneItems[zoneId].add(leatherCode);
  }
});

export const baseRngComponents = [
  301324, // first aid kit
  401208, // Tree of Life
  401209, // Meteorite
  401304, // Mithril
  401401, // VF Blood Sample
  401403, // force core
];

export const rngItems = [
  301324, // first aid kit
  401208, // Tree of Life
  401209, // Meteorite
  401304, // Mithril
  401401, // VF Blood Sample
  401403, // force core
];

// for stuff like glacial shoes or blazing dress it doesn't use the rng material directly.
// so this should be computed.
// const rngDescendents: number[] = [];
let anyEdits = true;
while (anyEdits) {
  anyEdits = false;
  for (let i = 0; i < allApiItems.length; i += 1) {
    const item = allApiItems[i];
    const { code } = item;
    if (!rngItems.includes(code)
      && (rngItems.includes(item.makeMaterial1) || rngItems.includes(item.makeMaterial2))
    ) {
      rngItems.push(code);
      anyEdits = true;
    }
  }
}

export const completedItems = new Set<number>();

function normalComponentType(one: ApiItem, two: number) {
  const twoDef = itemCodeToDef[two];
  if (twoDef == null) {
    return true;
  }
  return twoDef.itemType === ApiItemType.Misc
    || (one.weaponType === twoDef.weaponType
    && one.armorType === twoDef.armorType
    && one.itemType === twoDef.itemType
    && one.consumableType === twoDef.consumableType);
}

export const buildsInto: Record<number, number[]> = {};

function addBuildsInto(item: number, buildsIntoCode: number) {
  const entry: number[] = buildsInto[item] || [];
  entry.push(buildsIntoCode);
  buildsInto[item] = entry;
}

allApiItems.forEach((item) => {
  // I want materials for rng items to be target-able
  // so if it's made with an rng item, don't mark as an item used as a component
  addBuildsInto(item.makeMaterial1, item.code);
  addBuildsInto(item.makeMaterial2, item.code);
  const oneIsRngMaterial = rngItems.includes(item.makeMaterial1);
  const twoIsRngMaterial = rngItems.includes(item.makeMaterial2);
  if (!oneIsRngMaterial && !twoIsRngMaterial) {
    if (normalComponentType(item, item.makeMaterial1)) {
      itemsUsedAsComponent.add(item.makeMaterial1);
    }
    if (normalComponentType(item, item.makeMaterial2)) {
      itemsUsedAsComponent.add(item.makeMaterial2);
    }
  } else {
    // if at least 1 is an rng material, add the other as a completed item so it can be targeted.
    if (oneIsRngMaterial) {
      completedItems.add(item.makeMaterial2);
    }
    if (twoIsRngMaterial) {
      completedItems.add(item.makeMaterial1);
    }
  }
});

function computeNeededComponents(itemCode: number): Array<number> {
  const needed = [] as number[];
  const item = itemCodeToDef[itemCode];

  if (item === undefined) {
    // this is a special case for some items like scissors, leather, water,
    // and others that aren't in the ItemX json files that are otherwise used.
    itemsUsedAsComponent.add(itemCode);
    needed.push(itemCode);
  } else if (item.makeMaterial1 === 0) {
    needed.push(item.code);
  } else {
    computeNeededComponents(item.makeMaterial1).forEach((i) => needed.push(i));
    computeNeededComponents(item.makeMaterial2).forEach((i) => needed.push(i));
  }
  return needed;
}

export const basicComponentsNeeded: Record<number, Array<number>> = {};
allApiItems.forEach((item) => {
  basicComponentsNeeded[item.code] = computeNeededComponents(item.code);
});

allApiItems.forEach((item) => {
  if (!itemsUsedAsComponent.has(item.code)) {
    completedItems.add(item.code);
  }
});

// completedItems.add(202412);// Commander's Armor
// completedItems.add(202404);// Covert Agent Uniform
// completedItems.add(205402);// Glacial Ice
// completedItems.add(205203);// Flower of Fate
// completedItems.add(205403);// True Samachi Fire
completedItems.add(502208);// Recon Drones
// completedItems.add(205201);// White crane fan
// completedItems.add(502403);// Fire Trap
// completedItems.add(502402);// Stingburst
// completedItems.add(205304);// Laced Quiver
// completedItems.add(502308);// EMP Drone
// completedItems.add(302317);// Healing Potion
// completedItems.add(201404);// Crystal Tiara
// completedItems.add(205308);// Jolly Roger

export function computeBuildableAllZones(startingItems: number[]): number[] {
  return allApiItemCodes.filter((itemCode) => {
    const needed = basicComponentsNeeded[itemCode];
    for (let i = 0; i < needed.length; i += 1) {
      const neededCode = needed[i];
      if (!allFindableItems.has(neededCode) && !startingItems.includes(neededCode)) {
        return false;
      }
    }
    return true;
  });
}

const engMapping: Record<number, string> = require('./english-item-mapping.json') as Record<number, string>;

export const lowcaseEngMapping: [number, string][] = [];

const lowcaseEngMappingBlacklist = [
  501101, // regeneration cuff
];

Object.keys(engMapping).forEach(
  (key) => {
    const parsedKey = parseInt(key, 10);
    if (itemCodeToDef[parsedKey] != null && !lowcaseEngMappingBlacklist.includes(parsedKey)) {
      lowcaseEngMapping.push([parsedKey, engMapping[key].toLowerCase()]);
    }
  },
);
// and now do a custom sort to make it better for material selector
lowcaseEngMapping.sort(
  (a, b) => {
    const aVal = baseRngComponents.includes(a[0]) ? 0 : 1;
    const bVal = baseRngComponents.includes(b[0]) ? 0 : 1;
    if (aVal !== bVal) {
      return aVal - bVal;
    }
    return basicComponentsNeeded[a[0]].length - basicComponentsNeeded[b[0]].length;
  },
);

export function getEnglishName(code: number): string {
  return engMapping[code] || itemCodeToDef[code]?.name || 'unknown';
}

export function resolveItemImageName(code: number): string {
  // right now, all image names are based on
  // the english name since that's how it worked with lumiacraft.
  return getEnglishName(code).toLowerCase()
    .replaceAll(' ', '_')
    .replaceAll('\'', '_')
    .replaceAll('&', '-')
    .replaceAll('#', '_');
}
