#!/usr/bin/env python3
import json
from typing import Dict

sep_char = "┃"

item_mappings: Dict[str, str] = {}

with open("raw-mapping-file.txt") as input_file:
    for line in input_file:
        line = line.strip()
        if len(line) == 0:
            continue
        if line.startswith("Item/Name/"):
            almost_parsed = line[len("Item/Name/") :]
            item_id, english_name = almost_parsed.split(sep_char)
            assert (
                item_id not in item_mappings or item_mappings[item_id] == english_name
            ), f"duplicate ids for {english_name} (was {item_mappings[item_id]})"
            item_mappings[item_id] = english_name

with open("english-item-mapping.json", "w") as output_file:
    json.dump(item_mappings, output_file, indent=4, sort_keys=True)
