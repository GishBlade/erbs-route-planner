import { itemCodeToDef } from './ERBSJsonInterfaces';

export interface BuildStat {
  apiName: string,
  minValue: number,
  maxValue: number,
}

export function computeBuildStats(items: number[], adelaMode: boolean): Record<string, BuildStat> {
  const result: Record<string, BuildStat> = {};
  items.forEach((itemCode) => {
    const itemDef = itemCodeToDef[itemCode];
    if (itemDef == null || itemDef.properties == null) {
      return;
    }
    Object.entries(itemDef.properties).forEach(([apiStat, value]) => {
      const perLevelStat = apiStat.endsWith('ByLv');
      let resultKey = perLevelStat ? apiStat.substr(0, apiStat.length - 4) : apiStat;
      let multiplier = 1.0;
      if (adelaMode && resultKey === 'attackSpeedRatio') {
        resultKey = 'attackPower';
        multiplier = 0.8 * 100;
      }
      const buildStat = result[resultKey] || { apiName: resultKey, minValue: 0.0, maxValue: 0.0 };
      if (perLevelStat) {
        buildStat.maxValue += 20 * value * multiplier;
      } else {
        buildStat.minValue += value * multiplier;
        buildStat.maxValue += value * multiplier;
      }
      if (buildStat.minValue !== 0 || buildStat.maxValue !== 0) {
        result[resultKey] = buildStat;
      }
    });
  });
  return result;
}

const ordering: string[] = [
  'maxHp',
  'maxHpByLv',
  'defense',
  'defenseByLv',
  'moveSpeed',
  'outOfCombatMoveSpeed',
  'attackPower',
  'attackPowerByLv',
  'increaseBasicAttackDamage',
  'increaseBasicAttackDamageByLv',
  'increaseBasicAttackDamageRatioByLv',
  'increaseSkillDamage',
  'increaseSkillDamageByLv',
  'increaseSkillDamageRatio',
  'increaseSkillDamageRatioByLv',
  'cooldownLimit',
  'cooldownReduction',
  'attackRange',
  'attackSpeedRatio',
  'attackSpeedRatioByLv',
  'criticalStrikeChance',
  'criticalStrikeDamage',
  'healerGiveHpHealRatio',
  'hpHealedIncreaseRatio',
  'hpRegen',
  'hpRegenRatio',
  'lifeSteal',
  'maxSp',
  'normalLifeSteal',
  'penetrationDefense',
  'penetrationDefenseRatio',
  'preventBasicAttackDamaged',
  'preventBasicAttackDamagedByLv',
  'preventBasicAttackDamagedRatioByLv',
  'preventCriticalStrikeDamaged',
  'preventSkillDamaged',
  'preventSkillDamagedByLv',
  'preventSkillDamagedRatio',
  'preventSkillDamagedRatioByLv',
  'sightRange',
  'skillLifeSteal',
  'spRegen',
  'spRegenRatio',
  'trapDamageReduce',
  'trapDamageReduceRatio',
  'uniqueAttackRange',
  'uniqueCooldownLimit',
  'uniqueHpHealedIncreaseRatio',
  'uniqueTenacity',
];

export function computeSortedBuildStats(items: number[], adelaMode: boolean): BuildStat[] {
  const buildStats = Object.values(computeBuildStats(items, adelaMode));
  buildStats.sort((a, b) => {
    const aIdx = ordering.indexOf(a.apiName);
    const bIdx = ordering.indexOf(b.apiName);
    if (aIdx >= 0 && bIdx >= 0) {
      return aIdx - bIdx;
    }
    if (aIdx >= 0) {
      return -1;
    }
    if (bIdx >= 0) {
      return 1;
    }
    return 0;
  });
  return buildStats;
}
