// https://www.jameslmilner.com/post/workers-with-webpack-and-typescript/
declare module 'worker-loader!*' {
  class WebpackWorker extends Worker {
    constructor();
  }

  export default WebpackWorker;
}
