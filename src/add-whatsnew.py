#!/usr/bin/env python3
import argparse
import json
import datetime
from pathlib import Path
import time

parser = argparse.ArgumentParser()
parser.add_argument("--title", type=str, help="sets title for what's new message")
parser.add_argument("lines", type=str, nargs="+")
args = parser.parse_args()

date_string = datetime.datetime.now().strftime("%B %d, %Y")
epoch_thingy = time.time()
print("title:", args.title)
print("time :", date_string)
print("lines:", args.lines)

entries = []

entry_path = Path("./whats-new-entries.json")
if entry_path.exists():
    with open(entry_path, "r") as input_file:
        entries = json.load(input_file)

entries.insert(
    0,
    {
        "title": args.title,
        "lines": args.lines,
        "time": date_string,
        "epoch_time": epoch_thingy,
    },
)

with open(entry_path, "w") as output_file:
    json.dump(entries, output_file, indent=2)
