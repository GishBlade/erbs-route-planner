import React from 'react';
import {
  Badge, Box,
  createStyles, Divider,
  makeStyles,
  Theme, Typography,
} from '@material-ui/core';
import { ModalButton } from './components/ModalButton';
import usePersistedState from './persistentstate';

interface NewsEntry {
  title: string,
  lines: string[],
  time: string,
  epoch_time: number,
}

export const news = require('./whats-new-entries.json') as NewsEntry[];

// noinspection JSUnusedLocalSymbols
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const useStyles = makeStyles((theme: Theme) => createStyles(
  {
    root: {
      width: '800px',
      height: '800px',
      size: '10px',
      padding: 10,
      overflowY: 'auto',
    },
    newsLine: {
      margin: 10,
    },
  },
));

export default function WhatsNewButton() {
  const classes = useStyles();
  const [numNewsSeen, setNumNewsSeen] = usePersistedState('whats-new-count', news.length);
  const numNewPieces = news.length - numNewsSeen;
  return (
    <Badge color="error" badgeContent={numNewPieces} anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}>
      <ModalButton
        onClose={() => setNumNewsSeen(news.length)}
        buttonText="What's New?"
        modal={(
          <Box className={classes.root}>
            {news.map((newsPiece, index) => (
              <Box>
                {index === 0 ? '' : <Divider />}
                <Typography variant="h5">
                  {index < numNewPieces ? '(NEW) ' : ''}
                  {newsPiece.title}
                </Typography>
                {newsPiece.lines.map((line) => (
                  <Typography className={classes.newsLine}>{line}</Typography>
                ))}
              </Box>
            ))}
          </Box>
        )}
      />
    </Badge>
  );
}
