import React, { useEffect, useState } from 'react';
import {
  Box,
  Grid,
  Popover, TextField, Typography,
} from '@material-ui/core';
import { ItemIcon } from './ItemIcon';
import { lowcaseEngMapping } from '../erbs-data/ERBSJsonInterfaces';

export interface MaterialSelectorProps {
  selected?: number|null,
  setSelected: (code: number|null) => void,
  disabled?: boolean,
}

export function MaterialSelector(
  {
    selected,
    setSelected,
    disabled = false,
  }: MaterialSelectorProps,
) {
  const [anchorEl, setAnchorEl] = useState(null as any);
  const [query, setQuery] = useState('');
  const [filteredItems, setFilteredItems] = useState([] as number[]);
  useEffect(() => {
    setFilteredItems(
      lowcaseEngMapping
        .filter((entry: [number, string]) => entry[1].includes(query))
        .map(([code]: [number, string]) => code),
    );
  }, [query]);
  const maxItemsToRender = 56;

  const open = Boolean(anchorEl);
  return (
    <div>
      <ItemIcon
        disabled={disabled}
        item={selected || 0}
        onClick={(event) => {
          setAnchorEl(anchorEl || event.currentTarget);
          // setPopperOpen(!popperOpen);
        }}
        onRightClick={() => {
          setSelected(null);
        }}
      />
      <Popover
        open={open}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
        id="testing-popper"
        anchorEl={anchorEl}
        onClose={() => setAnchorEl(null)}
      >
        <Box style={{ width: 400, padding: 10 }}>
          <TextField
            autoFocus
            label="Filter"
            value={query}
            onChange={(ev) => setQuery(ev.target.value)}
          />
          <Typography>
            {Math.max(0, filteredItems.length - maxItemsToRender)}
            {' '}
            Items Not Shown. Use Filter Above to Find Things.
          </Typography>
          <Grid container>
            {filteredItems.slice(0, Math.min(maxItemsToRender, filteredItems.length))
              .map((code, idx) => (
                <Grid item key={`material-selector-${code * (idx + 1)}`}>
                  <ItemIcon
                    disabled={disabled}
                    item={code}
                    onClick={() => {
                      setAnchorEl(null);
                      setSelected(code);
                    }}
                  />
                </Grid>
              ))}
          </Grid>
        </Box>
      </Popover>
    </div>
  );
}
