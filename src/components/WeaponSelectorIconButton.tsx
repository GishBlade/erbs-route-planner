import React, { useState } from 'react';
import {
  Badge,
  Button,
  Popover,
} from '@material-ui/core';
import { WeaponSelectorGrid } from './WeaponSelectorGrid';

export interface WeaponSelectorItemProps {
  selected: string[]
  selectOneOnly?: boolean,
  setSelected: (newVal: string[]) => void,
}

export function WeaponSelectorIconButton({
  selected,
  selectOneOnly = false,
  setSelected,
}: WeaponSelectorItemProps) {
  const [anchorEl, setAnchorEl] = useState(null as any);
  const open = Boolean(anchorEl);
  const iconSize = 32;
  const iconStyle = { width: iconSize, height: iconSize };
  let buttonContent;
  if (selected.length === 0) {
    buttonContent = '?';
  } else {
    const weaponName = selected[0];
    const imgSrc = `weapon-icons/${weaponName.toLowerCase()}.png`;
    buttonContent = (
      <img
        alt={weaponName}
        src={imgSrc}
        style={iconStyle}
      />
    );
  }
  const buttonWidth = iconSize + 10;
  return (
    <div>
      <Badge
        anchorOrigin={{ horizontal: 'right', vertical: 'top' }}
        badgeContent={selected.length > 1 ? selected.length : 0}
        color="primary"
      >
        <Button
          onClick={(event) => setAnchorEl(anchorEl || event.currentTarget)}
          style={{ marginBottom: 3, minWidth: buttonWidth, width: buttonWidth }}
        >
          {buttonContent}
        </Button>
      </Badge>
      <Popover
        open={open}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
        id="testing-popper"
        anchorEl={anchorEl}
        onClose={() => setAnchorEl(null)}
      >
        <div style={{ width: 340 }}>
          <WeaponSelectorGrid
            selected={selected}
            selectOneOnly={selectOneOnly}
            setSelected={(val) => {
              if (selectOneOnly) {
                setAnchorEl(null);
              }
              setSelected(val);
            }}
          />
        </div>
      </Popover>
    </div>
  );
}
