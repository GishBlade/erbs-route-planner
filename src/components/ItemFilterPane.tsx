import React from 'react';
import {
  Chip,
  Divider, FormControl, FormControlLabel, FormGroup, Switch,
} from '@material-ui/core';
import {
  ApiArmorType, completedItems,
  ConsumableType,
  itemCodeToDef, SpecialItemType, weaponInfo,
  WeaponType,
} from '../erbs-data/ERBSJsonInterfaces';
import { WeaponSelectorGrid } from './WeaponSelectorGrid';

export interface ItemFilterRowProps {
  enumKey: object,
  currentlySelected: string[]|null|undefined,
  setVals: (newVal: string[]) => void,
  minWidth?: number,
}

export function ItemFilterRow({
  enumKey, currentlySelected, setVals, minWidth = 0,
}: ItemFilterRowProps) {
  return (
    <div>
      {Object.keys(enumKey).map((it) => {
        const contained = currentlySelected?.includes(enumKey[it]);
        return (
          <Chip
            variant={contained ? 'default' : 'outlined'}
            label={weaponInfo[it]?.englishName || it}
            style={{ margin: 5, minWidth: minWidth || 0 }}
            color={contained ? 'primary' : 'default'}
            onClick={() => {
              if (contained) {
                setVals(currentlySelected?.filter(
                  (item) => item !== it,
                ) || []);
              } else {
                setVals(
                  currentlySelected?.concat([enumKey[it]]) || [enumKey[it]],
                );
              }
            }}
          />
        );
      })}
    </div>
  );
}

export enum FilterPaneElement {
  Weapon,
  ArmorType,
  FoodType,
  MiscType,
  CompletedItemSwitch,
}

export interface ItemFilterParams {
  consumableType?: ConsumableType[]|null,
  armorType?: ApiArmorType[]|null,
  specialItemType?: SpecialItemType[]|null,
  weaponType?: WeaponType[]|null,
  allowIncompleteItems?: boolean,
}

export const defaultFilterPaneElements = [
  FilterPaneElement.Weapon,
  FilterPaneElement.ArmorType,
  FilterPaneElement.FoodType,
  FilterPaneElement.MiscType,
  FilterPaneElement.CompletedItemSwitch,
];

export interface ItemFilterPaneProps {
  filterParams: ItemFilterParams,
  setFilterParams: (ItemFilterParams) => void,
  filterPaneElements?: FilterPaneElement[],
}

export function ItemFilterPane(
  {
    filterParams,
    setFilterParams,
    filterPaneElements = defaultFilterPaneElements,
  }: ItemFilterPaneProps,
) {
  return (
    <div>
      {!filterPaneElements?.includes(FilterPaneElement.Weapon) ? '' : (
        <>
          <WeaponSelectorGrid
            selected={filterParams.weaponType || []}
            setSelected={(val) => setFilterParams({ ...filterParams, weaponType: val })}
          />
          <Divider />
        </>
      )}
      {!filterPaneElements?.includes(FilterPaneElement.ArmorType) ? '' : (
        <>
          <ItemFilterRow
            enumKey={ApiArmorType}
            currentlySelected={filterParams.armorType}
            setVals={(val) => setFilterParams({ ...filterParams, armorType: val })}
          />
          <Divider />
        </>
      )}
      {!filterPaneElements?.includes(FilterPaneElement.FoodType) ? '' : (
        <>
          <ItemFilterRow
            enumKey={ConsumableType}
            currentlySelected={filterParams.consumableType}
            setVals={(val) => setFilterParams({ ...filterParams, consumableType: val })}
          />
          <Divider />
        </>
      )}
      {!filterPaneElements?.includes(FilterPaneElement.MiscType) ? '' : (
        <>
          <ItemFilterRow
            enumKey={SpecialItemType}
            currentlySelected={filterParams.specialItemType}
            setVals={(val) => setFilterParams({ ...filterParams, specialItemType: val })}
          />
          <Divider />
        </>
      )}
      {!filterPaneElements?.includes(FilterPaneElement.CompletedItemSwitch) ? '' : (
        <>
          <FormControl>
            <FormGroup>
              <FormControlLabel
                control={(
                  <Switch
                    checked={filterParams.allowIncompleteItems}
                    onChange={() => setFilterParams(
                      { ...filterParams, allowIncompleteItems: !filterParams.allowIncompleteItems },
                    )}
                  />
                )}
                label="Show Incomplete Items"
              />
            </FormGroup>
          </FormControl>
          <Divider />
        </>
      )}
    </div>
  );
}

export function filterItems(itemCodes: number[], filter: ItemFilterParams): number[] {
  return itemCodes.filter((code) => {
    const itemDef = itemCodeToDef[code];
    if (!filter.allowIncompleteItems && !completedItems.has(code)) {
      return false;
    }
    if (
      filter.weaponType != null
      && itemDef.weaponType != null
      && filter.weaponType.length > 0 && filter.weaponType.includes(itemDef.weaponType)
    ) {
      return true;
    }
    if (
      filter.armorType != null
      && itemDef.armorType != null
      && filter.armorType.length > 0 && filter.armorType.includes(itemDef.armorType)
    ) {
      return true;
    }
    if (
      filter.consumableType != null
      && itemDef.consumableType != null
      && filter.consumableType.length > 0 && filter.consumableType.includes(itemDef.consumableType)
    ) {
      return true;
    }
    if (
      filter.specialItemType != null
      && itemDef.specialItemType != null
      // eslint-disable-next-line max-len
      && filter.specialItemType.length > 0 && filter.specialItemType.includes(itemDef.specialItemType)
    ) {
      return true;
    }
    return false;
  });
}
