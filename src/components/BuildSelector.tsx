import React, { useEffect, useState } from 'react';
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Button,
  Divider,
  Grid,
  LinearProgress,
  ListItem,
  ListItemText,
  Typography,
} from '@material-ui/core';
import _ from 'lodash';
import { FixedSizeList } from 'react-window';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
// eslint-disable-next-line import/no-webpack-loader-syntax
import AdvancedRoutingWorker from 'worker-loader!../worker';
import { ItemSelector } from './ItemSelector';
import {
  ApiArmorType,
  defaultStartingItems,
  weaponInfo,
  WeaponType,
  zoneIdToName,
  zoneItems,
} from '../erbs-data/ERBSJsonInterfaces';
import { multiItemRouting } from '../route-generation/BaselineRouteGeneration';
import { ComponentHelp } from './ComponentHelp';
import computeBasicComponentsNeeded, {
  NeededBuildItems,
} from '../RoutingStatistics';
import { ItemIcon, itemIconWidth } from './ItemIcon';
import { MaterialSelector } from './MaterialSelector';
import { RenderIf } from './RenderIf';
import { BuildStatRender } from './BuildStatRender';
import {
  advancedMultiItemRouting,
  AdvancedRoutingConfig, AdvancedRoutingState,
} from '../route-generation/AdvancedRouteGeneration';
import { RoutingAlgorithm } from '../route-generation/RouteGenerationLib';
import { FilterPaneElement } from './ItemFilterPane';
import { WeaponSelectorIconButton } from './WeaponSelectorIconButton';
import { ModalButton } from './ModalButton';
import { AdvancedRoutingConfigurationPanel } from './AdvancedRoutingConfigurationPanel';

export interface CharacterRoutingInfo {
  selectedRoute: number[],
  startingWeapons: string[],
  weaponFilter: string[],
  teammateBringsItems: number[],
  selectedItems: number[],
  advancedRoutingConfig: AdvancedRoutingConfig,
}

export interface BuildSelectorProps {
  routingInfo: CharacterRoutingInfo,
  setRoutingInfo: (newVal: CharacterRoutingInfo) => void,
  outlineColor: string,
  characterIndex: number,
  currentlyActive: number,
  setActiveCharacter: (index: number) => void,
  allRoutingInfo: CharacterRoutingInfo[],
  routingAlgorithm: RoutingAlgorithm,
}

export interface RenderRowProps {
  index: number,
  style: any,
}

interface MissingItemThing {
  code: number,
  amount: number,
}

export const buildSelectorWidth = 370;
export const buildSelectorMarginAmount = 5;
const lookingForRouteMessage = 'looking for routes...';

export function BuildSelector(
  {
    routingInfo,
    setRoutingInfo,
    outlineColor,
    characterIndex,
    allRoutingInfo,
    currentlyActive,
    setActiveCharacter,
    routingAlgorithm,
  }: BuildSelectorProps,
) {
  const {
    selectedRoute,
    startingWeapons,
    weaponFilter,
  } = routingInfo;
  const [selectedItems, setSelectedItems] = useState(
    [null, null, null, null, null, null] as (number|null)[],
  );
  const [adelaMode, setAdelaMode] = useState(false);
  const [foundRoutes, setFoundRoutes] = useState([] as number[][]);
  const [neededComponents, setNeededComponents] = useState(
    { basicsNeeded: {}, multiYieldNeeded: {}, multiYieldExtras: {} } as NeededBuildItems,
  );
  const [missingComponents, setMissingComponents] = useState([] as MissingItemThing[]);
  const [componentsPeopleCanBring, setComponentsPeopleCanBring] = useState([] as number[][]);
  const [foundRoutingStates, setFoundRoutingStates] = useState([] as AdvancedRoutingState[]);
  const [routeGenMessage, setRouteGenMessage] = useState('');
  const generationInProgress = routeGenMessage === lookingForRouteMessage;
  useEffect(() => {
    const canFind = allRoutingInfo.map((info, index) => {
      if (index === characterIndex) {
        return [];
      }
      return missingComponents.filter((missingItem) => {
        for (let i = 0; i < info.selectedRoute.length; i += 1) {
          if (zoneItems[info.selectedRoute[i]].has(missingItem.code)) {
            return true;
          }
        }
        return false;
      }).map((it) => it.code);
    });
    setComponentsPeopleCanBring(canFind);
  }, [allRoutingInfo, characterIndex, missingComponents]);
  const { teammateBringsItems } = routingInfo;
  const startingItems = defaultStartingItems.concat(teammateBringsItems);
  useEffect(() => {
    if (selectedRoute.length > 0 && selectedItems.length > 0) {
      const routeResult = advancedMultiItemRouting(
        routingInfo.selectedItems,
        defaultStartingItems.concat(startingWeapons.map((it) => weaponInfo[it].startingWeapon)),
        1,
        {
          ...routingInfo.advancedRoutingConfig,
          inventoryProtection: false,
          zoneConstraints: selectedRoute.map((it) => [it]),
          maxZones: selectedRoute.length,
          returnIfMax: true,
          allZonesHaveTP: true,
          ignoreNoValueZones: false,
        },
        weaponFilter,
      );
      if (routeResult.length === 1) {
        const routingStates = routeResult[0].states;
        const missingCounts: Record<number, MissingItemThing> = {};
        routingStates[routingStates.length - 1].basicComponentsNeeded.forEach((it) => {
          const { code } = it;
          const itemThing = missingCounts[code] || { code, amount: 0 };
          itemThing.amount += it.amount;
          missingCounts[code] = itemThing;
        });
        const missing = Object.values(missingCounts);
        missing.sort((a, b) => -(a.amount - b.amount));
        setMissingComponents(missing);
        setFoundRoutingStates(routingStates);
      }
    } else {
      setMissingComponents([]);
      setFoundRoutingStates([]);
    }
  }, [routingInfo.advancedRoutingConfig, routingInfo.selectedItems, weaponFilter,
    selectedItems, startingWeapons, teammateBringsItems, selectedRoute]);

  function computeBadgeNumber(selectionIndex: number, fieldName: string) {
    const badgeNumber = selectedItems[selectionIndex] == null ? null
      : foundRoutingStates.map((it, index) => (
        it.inventory[fieldName] === selectedItems[selectionIndex] ? index + 1 : null
      )).filter((it) => it != null)
        .reduce((a, b) => Math.min(a as number, b as number), 1000);
    if (badgeNumber === 1000) {
      return null;
    }
    return badgeNumber;
  }
  function neededItemsRender(
    codes: Record<number, number>, title: string, addCountToTitle = false,
  ) {
    const entries = Object.entries(codes);
    if (entries.length === 0) {
      return '';
    }
    let total = 0;
    entries.forEach((it) => { total += it[1]; });
    entries.sort((a, b) => -(a[1] - b[1]));
    return (
      <Grid item key={title}>
        <Typography align="left" style={{ marginBottom: 0 }}>
          {addCountToTitle ? `${total} ` : ''}
          {title}
        </Typography>
        <Grid container>
          {entries.length === 0
            ? ''
            : entries.map(
              ([itemCode, amount]) => (
                <ItemIcon
                  item={parseInt(itemCode, 10)}
                  blueBadgeNumber={amount}
                />
              ),
            )}
        </Grid>
      </Grid>
    );
  }

  useEffect(() => {
    setNeededComponents(computeBasicComponentsNeeded(
      selectedItems.filter((it) => it != null) as number[],
    ));
  }, [selectedItems]);

  function generateRoutes() {
    const filteredItems = selectedItems.filter((it) => it != null) as number[];
    if (filteredItems.length > 0) {
      if (routingAlgorithm === 'simple') {
        setFoundRoutes(multiItemRouting(filteredItems, defaultStartingItems, 20));
      } else {
        setFoundRoutes([]);
        setRouteGenMessage(lookingForRouteMessage);
        const worker = new AdvancedRoutingWorker();
        worker.postMessage([filteredItems,
          defaultStartingItems.concat(startingWeapons.map((it) => weaponInfo[it].startingWeapon)),
          20,
          routingInfo.advancedRoutingConfig,
          weaponFilter || []]);
        worker.onmessage = (event) => {
          const advancedRoutingResult = event.data;
          const actualResults = advancedRoutingResult.map((it) => it.zones);
          setFoundRoutes(
            actualResults,
          );
          if (actualResults.length === 0) {
            if (routingInfo.advancedRoutingConfig.enableLootDistConstraint) {
              setRouteGenMessage('Didn\'t find anything.\nYou may need to disable "Only Use 1 Loot Cluster Per Zone" in ROUTE GEN CONFIG to get routes for this build.');
            } else {
              setRouteGenMessage('Didn\'t find anything');
            }
          } else {
            setRouteGenMessage('');
          }
        };
      }
    } else {
      setFoundRoutes([]);
      setRouteGenMessage('');
    }
  }

  function selectItemAtIndex(code: number|null, index: number) {
    const copy = _.cloneDeep(selectedItems);
    if (index >= selectedItems.length) {
      copy.push(code);
    } else {
      copy[index] = code;
    }
    const filteredItems = copy.filter((val, idx) => idx < 6 || val != null);
    setSelectedItems(filteredItems);
    setRoutingInfo(
      { ...routingInfo, selectedItems: filteredItems.filter((it) => it != null) as number[] },
    );
  }

  function selectTeamItemAtIndex(code: number|null, index: number) {
    const copy = _.cloneDeep(teammateBringsItems) as (number|null)[];
    if (index >= teammateBringsItems.length) {
      copy.push(code);
    } else {
      copy[index] = code;
    }
    const filteredCopy = (copy.filter((val) => val != null) as number[]);
    // for now, just setting both teammateBringsItems and teamRareItems.
    // not currently sure of the best way to handle this due to simple routing.
    setRoutingInfo(
      {
        ...routingInfo,
        teammateBringsItems: filteredCopy,
        advancedRoutingConfig: {
          ...routingInfo.advancedRoutingConfig,
          teamRareItems: filteredCopy,
        },
      },
    );
  }

  function renderRouteRow(props: RenderRowProps) {
    const { index, style } = props;
    const route = foundRoutes[index];
    return (
      <ListItem
        button
        style={style}
        key={index}
        onClick={() => {
          setRoutingInfo({ ...routingInfo, selectedRoute: route });
        }}
      >
        <ListItemText primary={`${index + 1}. ${route.map((itemId) => zoneIdToName[itemId]).join(' > ')}`} />
      </ListItem>
    );
  }

  const extraSelectedItems = selectedItems.slice(6);

  const containerWidth = buildSelectorWidth - (2 * buildSelectorMarginAmount - 2);
  const mainBuildWidth = itemIconWidth * 3;
  const barMargin = 4;
  const extraItemsWidth = itemIconWidth * 2;
  const buildWidth = mainBuildWidth + extraItemsWidth + barMargin + 3;
  const rngTeamWidth = containerWidth - buildWidth - barMargin - 10;
  return (
    <Box style={{
      width: containerWidth,
      margin: buildSelectorMarginAmount,
      padding: 0,
      outlineStyle: 'solid',
      outlineColor,
      borderRadius: 5,
    }}
    >
      <Box>
        <Button
          variant={characterIndex === currentlyActive ? 'contained' : 'outlined'}
          onClick={() => setActiveCharacter(characterIndex)}
        >
          Active
        </Button>
        <ComponentHelp
          title={<Typography>Right click to remove an item</Typography>}
          dialogHelp={(
            <Box>
              <Typography>You can right-click to remove an item</Typography>
              <Typography>
                The item selectors will try to filter
                {' '}
                out items not buildable (e.g. as a result of not having meteorite).
              </Typography>
              <Typography>
                You can click the routes in the Route Generation
                {' '}
                section to over-ride the route on the map.
              </Typography>
            </Box>
          )}
        />
      </Box>
      <Grid container direction="row" style={{ width: containerWidth }}>
        <Grid container direction="column" style={{ width: buildWidth }}>
          <Grid item>
            <Typography align="center" style={{ width: buildWidth }}>Items to Build</Typography>
          </Grid>
          <Grid item>
            <Grid container direction="row">
              <Grid item key="main build area" style={{ width: mainBuildWidth }}>
                <Grid container direction="row">
                  <Grid item key="weapon selector">
                    <ItemSelector
                      disabled={generationInProgress}
                      blueBadgeNumber={computeBadgeNumber(0, 'weapon')}
                      startingItems={startingItems}
                      selected={selectedItems[0]}
                      hideFilter
                      setSelected={(code) => selectItemAtIndex(code, 0)}
                      overrideFilter={{
                        allowIncompleteItems: true,
                        weaponType: (weaponFilter as WeaponType[]),
                      }}
                      placeholder="weapon"
                    />
                  </Grid>
                  <Grid item key="chest selector">
                    <ItemSelector
                      disabled={generationInProgress}
                      blueBadgeNumber={computeBadgeNumber(1, 'chest')}
                      startingItems={startingItems}
                      initialFilter={
                        { armorType: [ApiArmorType.Chest], allowIncompleteItems: true }
                      }
                      filterPaneElements={[FilterPaneElement.CompletedItemSwitch]}
                      expandFilterByDefault
                      selected={selectedItems[1]}
                      setSelected={(code) => selectItemAtIndex(code, 1)}
                      placeholder="chest"
                    />
                  </Grid>
                  <Grid item key="head selector">
                    <ItemSelector
                      disabled={generationInProgress}
                      blueBadgeNumber={computeBadgeNumber(2, 'head')}
                      startingItems={startingItems}
                      initialFilter={{ armorType: [ApiArmorType.Head], allowIncompleteItems: true }}
                      filterPaneElements={[FilterPaneElement.CompletedItemSwitch]}
                      expandFilterByDefault
                      selected={selectedItems[2]}
                      setSelected={(code) => selectItemAtIndex(code, 2)}
                      placeholder="head"
                    />
                  </Grid>
                  <Grid container direction="row">
                    <Grid item key="arm selector">
                      <ItemSelector
                        disabled={generationInProgress}
                        blueBadgeNumber={computeBadgeNumber(3, 'arm')}
                        startingItems={startingItems}
                        initialFilter={
                          { armorType: [ApiArmorType.Arm], allowIncompleteItems: true }
                        }
                        filterPaneElements={[FilterPaneElement.CompletedItemSwitch]}
                        expandFilterByDefault
                        selected={selectedItems[3]}
                        setSelected={(code) => selectItemAtIndex(code, 3)}
                        placeholder="arm"
                      />
                    </Grid>
                    <Grid item key="leg selector">
                      <ItemSelector
                        disabled={generationInProgress}
                        blueBadgeNumber={computeBadgeNumber(4, 'leg')}
                        startingItems={startingItems}
                        initialFilter={
                          { armorType: [ApiArmorType.Leg], allowIncompleteItems: true }
                        }
                        filterPaneElements={[FilterPaneElement.CompletedItemSwitch]}
                        expandFilterByDefault
                        selected={selectedItems[4]}
                        setSelected={(code) => selectItemAtIndex(code, 4)}
                        placeholder="leg"
                      />
                    </Grid>
                    <Grid item key="trinket selector">
                      <ItemSelector
                        disabled={generationInProgress}
                        blueBadgeNumber={computeBadgeNumber(5, 'trinket')}
                        startingItems={startingItems}
                        initialFilter={
                          { armorType: [ApiArmorType.Trinket], allowIncompleteItems: true }
                        }
                        filterPaneElements={[FilterPaneElement.CompletedItemSwitch]}
                        expandFilterByDefault
                        selected={selectedItems[5]}
                        setSelected={(code) => selectItemAtIndex(code, 5)}
                        placeholder="trinket"
                      />
                    </Grid>
                  </Grid>
                </Grid>
              </Grid>
              <Grid item style={{ marginLeft: barMargin }} key="main-vs-extra-divider">
                <Divider orientation="vertical" />
              </Grid>
              <Grid item>
                <Grid container style={{ width: extraItemsWidth }}>
                  {extraSelectedItems.map((it, index) => (
                    <Grid item key={`extra selected items ${index * (it || -7)}`}>
                      <ItemSelector
                        disabled={generationInProgress}
                        startingItems={startingItems}
                        selected={selectedItems[index + 6]}
                        setSelected={(code) => {
                          selectItemAtIndex(code, index + 6);
                        }}
                      />
                    </Grid>
                  ))}
                  <Grid item key="additinoal-item-for-extra-stuff">
                    <ItemSelector
                      disabled={generationInProgress}
                      startingItems={startingItems}
                      selected={null}
                      setSelected={(code) => selectItemAtIndex(code, selectedItems.length)}
                    />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
        <Grid item style={{ marginLeft: barMargin }} key="teammate-can-bring-divider">
          <Divider orientation="vertical" />
        </Grid>
        <Grid item style={{ width: rngTeamWidth }} key="teammate-can-bring">
          <Typography align="center" style={{ marginLeft: 10 }}>
            Team/Rare
            {' '}
            <ComponentHelp title={(
              <Typography>
                Items given by team mates or rare resources found.
              </Typography>
            )}
            />
          </Typography>
          <Grid container>
            {teammateBringsItems.map((it, index) => (
              <Grid item key={`selected-material-${index * (it || -7)}`}>
                <MaterialSelector
                  disabled={generationInProgress}
                  key={`selected-material-${index * (it || -7)}-selector`}
                  selected={teammateBringsItems[index]}
                  setSelected={(code) => {
                    selectTeamItemAtIndex(code, index);
                  }}
                />
              </Grid>
            ))}
            <Grid item key="material-selector-at-tend">
              <MaterialSelector
                disabled={generationInProgress}
                key="ending-selector-for-thingy"
                selected={null}
                setSelected={(code) => selectTeamItemAtIndex(code, teammateBringsItems.length)}
              />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Box>
        <Divider />
        <Grid container direction="row" style={{ marginTop: 5 }}>
          <Grid item>
            <WeaponSelectorIconButton
              selected={weaponFilter}
              setSelected={(val) => {
                if (startingWeapons.length === 0 || val.length === 1) {
                  setRoutingInfo({
                    ...routingInfo,
                    weaponFilter: val,
                    startingWeapons: [val[val.length - 1]],
                  });
                } else {
                  setRoutingInfo({ ...routingInfo, weaponFilter: val });
                }
              }}
            />
          </Grid>
          <Grid item>
            <Typography display="inline">
              Weapon
            </Typography>
          </Grid>
          <Grid item>
            <WeaponSelectorIconButton
              selected={startingWeapons}
              selectOneOnly
              setSelected={(val) => setRoutingInfo({ ...routingInfo, startingWeapons: val })}
            />
          </Grid>
          <Grid item>
            <Typography display="inline" style={{ width: 100 }}>
              Starting Weapon
            </Typography>
          </Grid>
        </Grid>
        <Divider />
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
          >
            <Typography variant="h6">
              Main Build Stat Totals
            </Typography>
          </AccordionSummary>
          <AccordionDetails>
            <BuildStatRender
              selectedItems={selectedItems.slice(0, 6)}
              adelaMode={adelaMode}
              showAdelaSwitch
              setAdelaMode={setAdelaMode}
            />
          </AccordionDetails>
        </Accordion>
        {missingComponents.length === 0 ? '' : (
          <>
            <Typography align="left" style={{ marginBottom: 0 }}>
              Missing
            </Typography>
            <Grid container>
              {missingComponents.map((missingItem, idx) => {
                const { code } = missingItem;
                return (
                  <Grid item key={`missing-component-${code * (idx + 1)}`}>
                    <ItemIcon
                      item={code}
                      blueBadgeNumber={missingItem.amount}
                      disabled={generationInProgress}
                      onClick={() => selectTeamItemAtIndex(code, teammateBringsItems.length)}
                    />
                  </Grid>
                );
              })}
            </Grid>
          </>
        )}
        {componentsPeopleCanBring.map((itemCodes, index) => {
          if (index !== characterIndex && itemCodes.length > 0) {
            return (
              <Grid container>
                <Grid item style={{ width: '100%' }}>
                  <Typography variant="h6">
                    Character
                    {' '}
                    {index + 1}
                    {' '}
                    Has Along Route
                  </Typography>
                </Grid>
                {itemCodes.map((itemCode) => (
                  <Grid item key={`char-${index + 1}-can-bring-${itemCode}`}>
                    <ItemIcon
                      item={itemCode}
                      disabled={generationInProgress}
                      onClick={() => selectTeamItemAtIndex(itemCode, teammateBringsItems.length)}
                    />
                  </Grid>
                ))}
              </Grid>
            );
          }
          return '';
        })}
        <Grid container>
          {neededItemsRender(neededComponents.basicsNeeded, 'Basic Components Needed for Build', true)}
          <Divider />
          {neededItemsRender(neededComponents.multiYieldNeeded, 'Multi-Stack Needed (e.g. steel sheet, stones)')}
          <Divider />
          {neededItemsRender(neededComponents.multiYieldExtras, 'Excess Multi-stack (if build completed solo)')}
          <Divider />
        </Grid>
      </Box>
      <Box>
        <Grid container direction="row">
          <Grid item>
            <Button
              color="primary"
              variant="contained"
              onClick={() => generateRoutes()}
              disabled={generationInProgress}
            >
              Generate Routes
            </Button>
          </Grid>
          <Grid item>
            <ModalButton
              buttonText="Route Gen Config"
              modal={(
                <AdvancedRoutingConfigurationPanel
                  value={routingInfo.advancedRoutingConfig}
                  callback={(newConfig) => setRoutingInfo(
                    { ...routingInfo, advancedRoutingConfig: newConfig },
                  )}
                />
              )}
            />
          </Grid>
        </Grid>
        <RenderIf
          condition={foundRoutes.length > 0}
          whenTrue={() => (
            <>
              <Typography style={{ margin: 5 }}>
                (Assumes Team/Rare items in 3rd zone.)
              </Typography>
              <FixedSizeList itemSize={45} height={300} itemCount={foundRoutes.length} width="100%">
                {renderRouteRow}
              </FixedSizeList>
            </>
          )}
          whenFalse={() => (
            <>
              {routeGenMessage.split('\n').map((message) => (
                <Typography style={{ margin: 5 }}>{message}</Typography>
              ))}
              {generationInProgress ? <LinearProgress style={{ marginBottom: 10 }} /> : ''}
            </>
          )}
        />
      </Box>
    </Box>
  );
}
