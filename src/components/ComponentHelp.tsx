import React, { useState } from 'react';
import {
  Button,
  createStyles, Dialog, DialogActions, DialogContent, IconButton,
  makeStyles,
  Theme, Tooltip, Typography,
} from '@material-ui/core';
import HelpIcon from '@material-ui/icons/Help';

export interface ComponentHelpProps {
  title: string|Node|JSX.Element,
  dialogHelp?: Node|null|JSX.Element,
  maxWidth?: 'sm'|'xs'|'md'|'lg'|'xl',
  iconSize?: 'small'|'medium',
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const useStyles = makeStyles((theme: Theme) => createStyles(
  {
    helpIcon: {
      margin: 0,
      // float: 'right',
    },
  },
));

export function ComponentHelp({
  title, dialogHelp = null, maxWidth = 'sm', iconSize = 'medium',
}: ComponentHelpProps) {
  const classes = useStyles();
  const [showModal, setShowModal] = useState(false);
  return (
    <>
      <Tooltip title={(
        <>
          {dialogHelp != null ? <Typography>(Click this icon for more details)</Typography> : ''}
          {title}
        </>
      )}
      >
        <IconButton
          style={{ padding: 0, margin: 0, verticalAlign: 'middle' }}
          size={iconSize}
          className={classes.helpIcon}
          onClick={() => setShowModal(true)}
        >
          <HelpIcon />
        </IconButton>
      </Tooltip>
      <Dialog open={showModal} onClose={() => setShowModal(false)} maxWidth={maxWidth}>
        <DialogContent>{dialogHelp || title}</DialogContent>
        <DialogActions>
          <Button onClick={() => setShowModal(false)}>Close</Button>
        </DialogActions>
      </Dialog>
    </>
  );
}
