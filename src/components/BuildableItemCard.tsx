import React from 'react';
import {
  Card, Divider, Grid, Typography,
} from '@material-ui/core';
import { ItemIcon } from './ItemIcon';
import {
  ApiArmorType,
  ApiItemGrade,
  ConsumableType,
  itemCodeToDef,
  SpecialItemType,
  weaponInfo,
  WeaponType,
} from '../erbs-data/ERBSJsonInterfaces';
import { BuildableItemResult } from '../RoutingStatistics';
import sortItemList from '../sorting-utils';

export interface ItemCardProps {
  items: BuildableItemResult[],
  weaponTypes: WeaponType[]|null,
  debugMode: boolean,
  width?: number,
}

export function BuildableItemCard({
  items, width = 360, weaponTypes = null, debugMode = false,
}: ItemCardProps) {
  const typeWidth = 75;
  const remainingWidth = width - typeWidth - 2;
  const itemBreakdown: Record<string, BuildableItemResult[]> = {};
  const keys = ['Head', 'Chest', 'Arm', 'Leg', 'Accessory', 'Foods', 'Drinks', 'Summon'];
  const weaponKeys: string[] = [];
  items.forEach((item) => {
    const itemDef = itemCodeToDef[item.code];
    if (itemDef.itemGrade !== ApiItemGrade.Common) {
      let key: string|null = null;
      if (itemDef.consumableType === ConsumableType.Beverage) {
        key = 'Drinks';
      } else if (itemDef.consumableType === ConsumableType.Food) {
        key = 'Foods';
      } else if (itemDef.specialItemType === SpecialItemType.Summon) {
        key = 'Summon';
      } else if (itemDef.armorType === ApiArmorType.Leg) {
        key = 'Leg';
      } else if (itemDef.armorType === ApiArmorType.Arm) {
        key = 'Arm';
      } else if (itemDef.armorType === ApiArmorType.Head) {
        key = 'Head';
      } else if (itemDef.armorType === ApiArmorType.Chest) {
        key = 'Chest';
      } else if (itemDef.armorType === ApiArmorType.Trinket) {
        key = 'Accessory';
      } else if (
        itemDef.weaponType != null
        && weaponTypes != null
        && weaponTypes.includes(itemDef.weaponType)
      ) {
        key = weaponInfo[itemDef.weaponType].englishName;
      }
      if (key != null) {
        if (!keys.includes(key) && !weaponKeys.includes(key)) {
          weaponKeys.push(key);
        }
        const lst = itemBreakdown[key] || [];
        lst.push(item);
        itemBreakdown[key] = lst;
      }
    }
  });
  if (Object.keys(itemBreakdown).length === 0) {
    return (
      <Card style={{ width }}>
        <Typography>
          Nothing to show.
        </Typography>
      </Card>
    );
  }
  const allKeys = weaponKeys.concat(...keys);
  return (
    <Card style={{ margin: 0 }}>
      <Grid container direction="column" style={{ width }}>
        {allKeys.map((itemKey, index) => {
          const itemList = itemBreakdown[itemKey];
          if (itemList == null || itemList.length === 0) {
            return '';
          }
          sortItemList(itemList, (item) => itemCodeToDef[item.code], false, true, true);
          return (
            <Grid item>
              {index === 0 ? '' : <Divider />}
              <Grid container direction="row">
                <Grid item style={{ width: typeWidth }}>
                  <Typography>
                    {itemKey}
                  </Typography>
                </Grid>
                <Grid item style={{ width: remainingWidth }}>
                  <Grid container>
                    {itemList.map((item) => (
                      <Grid item style={{ marginLeft: 3 }}>
                        <ItemIcon
                          item={item.code}
                          debugMode={debugMode}
                          blueBadgeNumber={item.zoneNumber}
                          redBadgeNumber={item.zoneNumberWithWeapon}
                        />
                      </Grid>
                    ))}
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          );
        })}
      </Grid>
    </Card>
  );
}
