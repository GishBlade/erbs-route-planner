import React from 'react';
import {
  Button,
  Grid,
  Tooltip,
  Typography,
} from '@material-ui/core';
import { weaponInfo, WeaponType } from '../erbs-data/ERBSJsonInterfaces';

export interface WeaponSelectorProps {
  selected: string[]
  selectOneOnly?: boolean,
  setSelected: (newVal: string[]) => void,
}

export function WeaponSelectorGrid(
  { selected, selectOneOnly = false, setSelected }: WeaponSelectorProps,
) {
  const iconSize = 32;
  const iconStyle = { width: iconSize, height: iconSize };
  return (
    <div>
      <Grid container>
        {Object.keys(WeaponType).map((weaponName) => {
          let imgSrc;
          const currentlySelected = selected.includes(weaponName);
          if (currentlySelected) {
            imgSrc = `weapon-icons/${weaponName.toLowerCase()}.png`;
          } else {
            imgSrc = `weapon-icons/grayscale/${weaponName.toLowerCase()}.png`;
          }
          const buttonWidth = iconSize + 10;
          return (
            <Grid item key={`weapon-selector-${weaponName}`}>
              <Tooltip title={<Typography>{weaponInfo[weaponName].englishName}</Typography>}>
                <Button
                  style={{ marginBottom: 3, minWidth: buttonWidth, width: buttonWidth }}
                  onClick={() => {
                    if (currentlySelected) {
                      if (selectOneOnly) {
                        setSelected([]);
                      } else {
                        setSelected(selected.filter((it) => it !== weaponName));
                      }
                    } else if (selectOneOnly) {
                      setSelected([weaponName]);
                    } else {
                      setSelected(selected.concat(weaponName));
                    }
                  }}
                >
                  <img
                    alt={weaponName}
                    src={imgSrc}
                    style={iconStyle}
                  />
                </Button>
              </Tooltip>
            </Grid>
          );
        })}
      </Grid>
    </div>
  );
}
