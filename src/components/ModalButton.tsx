import React from 'react';
import {
  Button, Card,
  Modal,
} from '@material-ui/core';

export interface ModalButtonProps {
  buttonText: string|Node|JSX.Element,
  modal: Node|JSX.Element,
  onClose?: undefined|(() => void),
}

export function ModalButton({ buttonText, modal, onClose }: ModalButtonProps) {
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => {
    if (onClose != null) {
      onClose();
    }
    setOpen(false);
  };

  return (
    <div>
      <Button onClick={handleOpen}>{buttonText}</Button>
      <Modal
        open={open}
        onClose={handleClose}
      >
        <Card style={{
          position: 'absolute',
          top: '50%',
          left: '50%',
          transform: 'translate(-50%, -50%)',
        }}
        >
          {modal}
        </Card>
      </Modal>
    </div>
  );
}
