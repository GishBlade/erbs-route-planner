// code mostly copy-pasted from example in https://material-ui.com/components/popover/
import React from 'react';
import {
  Box,
  createStyles,
  makeStyles, Popover,
  Theme,
} from '@material-ui/core';

export interface PopoverTooltipProps {
  children: Node|JSX.Element,
  title: Node|JSX.Element,
}

const useStyles = makeStyles((theme: Theme) => createStyles(
  {
    popover: {
      pointerEvents: 'none',
    },
    paper: {
      padding: theme.spacing(1),
    },
  },
));

export function PopoverTooltip({ children, title }: PopoverTooltipProps) {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handlePopoverOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);

  return (
    <div>
      <Box
        aria-owns={open ? 'mouse-over-popover' : undefined}
        aria-haspopup="true"
        onMouseEnter={handlePopoverOpen}
        onMouseLeave={handlePopoverClose}
      >
        {children}
      </Box>
      <Popover
        id="mouse-over-popover"
        className={classes.popover}
        classes={{
          paper: classes.paper,
        }}
        open={open}
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
        onClose={handlePopoverClose}
        disableRestoreFocus
      >
        {title}
      </Popover>
    </div>
  );
}
