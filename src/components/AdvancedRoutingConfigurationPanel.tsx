/* eslint-disable react/jsx-boolean-value */
import React from 'react';
import {
  Box, Checkbox, createStyles, Divider,
  IconButton, makeStyles,
  Table, TableBody, TableCell, TableRow, TextField, Theme,
  Tooltip, Typography,
} from '@material-ui/core';
import CachedIcon from '@material-ui/icons/Cached';
import _ from 'lodash';
import {
  AdvancedRoutingConfig,
  defaultAdvancedRoutingConfig,
} from '../route-generation/AdvancedRouteGeneration';
import { ComponentHelp } from './ComponentHelp';
import { ZoneSelector } from './ZoneSelector';
import { allZoneIds } from '../erbs-data/ERBSJsonInterfaces';

export interface AdvancedRoutingConfigRowProps {
  value: AdvancedRoutingConfig
  fieldName: string,
  label: string,
  callback?: ((config: AdvancedRoutingConfig) => void)|undefined,
  type: 'number'|'boolean',
  explanation: string|Node|JSX.Element,
  min?: number,
  max?: number,
}

function AdvancedRoutingConfigRow({
  type, label, value, fieldName, explanation, callback, max, min = 0,
}: AdvancedRoutingConfigRowProps) {
  function setValue(newValue: any) {
    if (callback != null) {
      let actualValue = newValue;
      if (max != null) {
        actualValue = Math.min(max, actualValue);
      }
      const newThing = { ...value };
      newThing[fieldName] = actualValue;
      callback(newThing);
    }
  }

  const inputStyle = { maxWidth: 50 };
  const inputValue = value[fieldName];
  let inputComponent;
  if (type === 'number') {
    inputComponent = (
      <TextField
        value={inputValue}
        inputProps={{ min, max }}
        type="number"
        style={inputStyle}
        onChange={(ev) => setValue(ev.target.value)}
      />
    );
  } else if (type === 'boolean') {
    inputComponent = (
      <Checkbox
        style={inputStyle}
        checked={inputValue}
        value={inputValue}
        onChange={() => setValue(!inputValue)}
      />
    );
  } else {
    inputComponent = (
      <Typography display="inline">
        ERROR. UNKNOWN THINGY!
      </Typography>
    );
  }
  const defaultValue = defaultAdvancedRoutingConfig[fieldName];
  return (
    <TableRow>
      <TableCell>
        <Tooltip title={<Typography>Load Default Value</Typography>}>
          <IconButton onClick={() => setValue(defaultValue)} disabled={inputValue === defaultValue}>
            <CachedIcon />
          </IconButton>
        </Tooltip>
      </TableCell>
      <TableCell>
        <ComponentHelp title={explanation} />
      </TableCell>
      <TableCell>
        {inputComponent}
      </TableCell>
      <TableCell>
        <Typography display="inline">{label}</Typography>
      </TableCell>
    </TableRow>
  );
}

export interface AdvancedRoutingConfigurationProps {
  value: AdvancedRoutingConfig
  callback?: ((config: AdvancedRoutingConfig) => void)|undefined,
}

// noinspection JSUnusedLocalSymbols
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const useStyles = makeStyles((theme: Theme) => createStyles(
  {
    root: {
      width: '800px',
      height: '800px',
      size: '10px',
      padding: 10,
      overflowY: 'auto',
    },
  },
));

export function AdvancedRoutingConfigurationPanel(
  { value, callback }: AdvancedRoutingConfigurationProps,
) {
  const classes = useStyles();
  function handleSetZone(zoneNum: number, zones: number[]) {
    if (callback != null) {
      const newZones: (null|number[])[] = _.clone(value.zoneConstraints || []);
      while (newZones.length < zoneNum) {
        newZones.push(null);
      }
      // can't just set to null since otherwise you can't use the None select.
      newZones[zoneNum] = zones;
      callback({ ...value, zoneConstraints: newZones });
    }
  }
  function getZone(zoneNum: number): number[] {
    if (value.zoneConstraints == null
      || value.zoneConstraints.length < zoneNum || value.zoneConstraints[zoneNum] == null) {
      return allZoneIds;
    }
    return value.zoneConstraints[zoneNum]!!;
  }
  return (
    <div className={classes.root}>
      <Typography variant="h4">Route Generation Options</Typography>
      <Typography>These options also affect inventory simulation</Typography>
      <Table size="small">
        <TableBody>
          <AdvancedRoutingConfigRow
            value={value}
            fieldName="inventoryProtection"
            label="Inventory Protection"
            type="boolean"
            callback={callback}
            explanation={(
              <Box>
                <Typography>
                  Ensures that your inventory does not get overfilled between zones.
                </Typography>
                <Typography>
                  Does not protect against in-zone inventory explosions.
                </Typography>
              </Box>
            )}
          />
          <AdvancedRoutingConfigRow
            value={value}
            fieldName="inventorySize"
            label="Inventory Size"
            type="number"
            min={1}
            max={10}
            callback={callback}
            explanation={(
              <Typography>
                This value is used as the max inventory size for inventory protection.
              </Typography>
            )}
          />
          <AdvancedRoutingConfigRow
            value={value}
            fieldName="maxZones"
            label="Max Zones"
            type="number"
            min={1}
            max={6}
            callback={callback}
            explanation={(
              <Typography>
                Maximum number of zones the route can use.
              </Typography>
            )}
          />
          <AdvancedRoutingConfigRow
            value={value}
            fieldName="allZonesHaveTP"
            label="Can Teleport from All Zones"
            type="boolean"
            callback={callback}
            explanation={(
              <Box>
                <Typography>
                  Pretend all zones have access to teleporters.
                </Typography>
                <Typography>
                  Otherwise, zones without teleporters only have access to neighboring zones.
                </Typography>
              </Box>
            )}
          />
          <AdvancedRoutingConfigRow
            value={value}
            fieldName="minLeatherZone"
            label="Minimum Leather Zone"
            type="number"
            min={1}
            max={6}
            callback={callback}
            explanation={(
              <Box>
                <Typography>
                  Minimum zone before leather should be collected.
                </Typography>
              </Box>
            )}
          />
          <AdvancedRoutingConfigRow
            value={value}
            fieldName="enableLootDistConstraint"
            label="Only Use 1 Loot Cluster Per Zone"
            type="boolean"
            callback={callback}
            explanation={(
              <Box>
                <Typography>
                  Makes routing account for only using 1 loot cluster per zone.
                </Typography>
                <Typography>
                  This will result in things like routing through
                  {' '}
                  {'Hospital->Forest'}
                  {' '}
                  for 3 feathers instead of using just 1 feather zone
                  {' '}
                  since Hospital has 1 feather/cluster while Forest has 2 feathers/cluster.
                </Typography>
              </Box>
            )}
          />
        </TableBody>
      </Table>
      {Array(value.maxZones || 1).fill(1)
        .map((ignored, zoneNum) => (
          <>
            {zoneNum > 0 ? <Divider /> : ''}
            <Typography variant="h5">
              Zone
              {' '}
              {zoneNum + 1}
            </Typography>
            <ZoneSelector
              zones={getZone(zoneNum)}
              setZones={(zones) => handleSetZone(zoneNum, zones)}
            />
          </>
        ))}
    </div>
  );
}
