import React from 'react';
import {
  Typography,
} from '@material-ui/core';
import { Tree, TreeNode } from 'react-organizational-chart';
import { itemCodeToDef } from '../erbs-data/ERBSJsonInterfaces';

export interface BuildTreeProps {
  itemCode: number,
  renderIcon: (code: number) => any,
}

function BuildNode({ itemCode, renderIcon }: BuildTreeProps) {
  const itemDef = itemCodeToDef[itemCode];
  if (itemDef.makeMaterial1 === 0) {
    return (
      <TreeNode label={renderIcon(itemCode)} />
    );
  }
  return (
    <TreeNode
      label={renderIcon(itemCode)}
    >
      <BuildNode itemCode={itemDef.makeMaterial1} renderIcon={renderIcon} />
      <BuildNode itemCode={itemDef.makeMaterial2} renderIcon={renderIcon} />
    </TreeNode>
  );
}

export function BuildTree({ itemCode, renderIcon }: BuildTreeProps) {
  const itemDef = itemCodeToDef[itemCode];
  if (itemDef == null) {
    return (
      <Typography>
        No Item Def
      </Typography>
    );
  }
  if (itemDef.makeMaterial1 === 0) {
    return (
      renderIcon(itemCode)
    );
  }
  return (
    <Tree
      label={renderIcon(itemCode)}
      lineWidth="4px"
      lineColor="white"
      lineHeight="15px"
    >
      <BuildNode itemCode={itemDef.makeMaterial1} renderIcon={renderIcon} />
      <BuildNode itemCode={itemDef.makeMaterial2} renderIcon={renderIcon} />
    </Tree>
  );
}
