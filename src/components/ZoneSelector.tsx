import React from 'react';
import {
  Button,
  Chip,
  Divider, Grid,
} from '@material-ui/core';
import { allZoneIds, zoneIdToName } from '../erbs-data/ERBSJsonInterfaces';

export interface ZoneSelectorProps {
  zones: (number[])|null,
  setZones: (newZones: number[]) => void,
}

export function ZoneSelector({ zones, setZones }: ZoneSelectorProps) {
  function highlighted(zoneNum: number): 'outlined'|'default' {
    return (zones != null && zones.includes(zoneNum)) ? 'default' : 'outlined';
  }
  return (
    <Grid container direction="row">
      <Grid item style={{ width: 150 }}>
        <Button
          onClick={() => setZones(allZoneIds)}
        >
          All
        </Button>
        <Button
          onClick={() => setZones([])}
        >
          None
        </Button>
      </Grid>
      <Grid item style={{ width: 20 }}>
        <Divider orientation="vertical" />
      </Grid>
      <Grid item style={{ width: 550 }}>
        <Grid container>
          {allZoneIds.map((zoneId) => {
            if (zoneId >= 16) { // want to ignore research center
              return '';
            }
            return (
              <Chip
                variant={highlighted(zoneId)}
                size="small"
                color="default"
                style={{ margin: 3 }}
                label={zoneIdToName[zoneId]}
                onClick={() => {
                  const currentZones = zones || [];
                  if (currentZones.includes(zoneId)) {
                    setZones(currentZones.filter((it) => it !== zoneId));
                  } else {
                    setZones(currentZones.concat([zoneId]));
                  }
                }}
              />
            );
          })}
        </Grid>
      </Grid>
    </Grid>
  );
}
