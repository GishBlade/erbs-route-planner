/* eslint-disable @typescript-eslint/no-unused-vars */
import React from 'react';

export interface RenderIfProps {
  condition: boolean,
  whenTrue: () => JSX.Element|null,
  whenFalse?: () => JSX.Element|null,
}

export function RenderIf(
  { condition, whenTrue, whenFalse = () => null }: RenderIfProps,
): JSX.Element|null {
  if (condition) {
    return whenTrue();
  }
  return whenFalse();
}
