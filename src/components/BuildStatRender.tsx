// import React from 'react';
import {
  FormControlLabel, Grid, Switch, Typography,
} from '@material-ui/core';
import React from 'react';
import { BuildStat, computeSortedBuildStats } from '../erbs-data/BuildStats';

const propertyIdLookupTable = {
  increaseSkillDamage: 'extraSkillDamage',
  increaseSkillDamageRatio: 'skillAmplification',
  spRegenRatio: 'spRegen',
  hpRegenRatio: 'hpRegen',
  preventSkillDamagedRatio: 'skillDamageReduction',
  preventSkillDamaged: 'flatSkillDamageReduction',
  increaseBasicAttackDamage: 'extraNormalAttackDamage',
  attackSpeedRatio: 'attackSpeed',
  penetrationDefenseRatio: 'defencePenetration',
  preventBasicAttackDamaged: 'normalAttackDamageReduction',
  preventCriticalStrikeDamaged: 'criticalDamageReduction',
  cooldownLimit: 'cooldownReductionCapIncrease',
  trapDamageReduceRatio: 'trapDamageReduction',
  influencePoint: 'skillAttackPower (Skill Amp)',
};

export function normNumericValue(value: number, valueIsPercentage: boolean): string {
  if (valueIsPercentage) {
    return `${(100 * value).toFixed(0)}%`;
  }
  // need to convert to/from number to avoid issues with stuff like 0.58000000001 move speed
  return `${parseFloat(value.toFixed(2))}`;
}

export function propertyIdToHumanString(fullStatInfo: BuildStat): string {
  let id = fullStatInfo.apiName;
  let lastDex = 0;
  let currentDex = 1;
  const words: Array<string> = [];
  const perLevelStat = id.endsWith('ByLv');
  if (perLevelStat) {
    id = id.substring(0, id.length - 4);
  }
  const lowcaseId = id.toLowerCase();
  const valueIsPercentage = lowcaseId.includes('ratio')
    || lowcaseId.includes('chance')
    || lowcaseId.includes('cooldownlimit')
    || lowcaseId.includes('criticalstrikedamage')
    || lowcaseId.includes('lifesteal')
    || lowcaseId.includes('cooldownreduction');
  if (propertyIdLookupTable[id] !== undefined) {
    id = propertyIdLookupTable[id];
  }
  while (currentDex < id.length) {
    if (id[currentDex] === id[currentDex].toUpperCase()) {
      const wordToAdd = id[lastDex].toUpperCase() + id.substring(lastDex + 1, currentDex);
      words.push(wordToAdd);
      lastDex = currentDex;
    }
    currentDex += 1;
  }
  // last one won't be gotten in loop
  words.push(id[lastDex].toUpperCase() + id.substring(lastDex + 1, currentDex));
  if (perLevelStat) {
    words.push('Per Level');
  }
  if (fullStatInfo.minValue !== fullStatInfo.maxValue) {
    const minFormatted = normNumericValue(fullStatInfo.minValue, valueIsPercentage);
    const maxFormatted = normNumericValue(fullStatInfo.maxValue, valueIsPercentage);
    return `${words.join(' ')}: ${minFormatted}-${maxFormatted}`;
  }
  return `${words.join(' ')}: ${normNumericValue(fullStatInfo.minValue, valueIsPercentage)}`;
}

export interface BuildStatRenderProps {
  selectedItems: (number|null)[],
  adelaMode?: boolean,
  showAdelaSwitch?: boolean,
  setAdelaMode?: (value: boolean) => void,
}

export function BuildStatRender(
  {
    selectedItems, adelaMode = false, setAdelaMode = () => {}, showAdelaSwitch = false,
  }: BuildStatRenderProps,
) {
  return (
    <Grid container direction="column">
      {showAdelaSwitch
        ? (
          <Grid item key="adela mode switch">
            <FormControlLabel
              style={{ width: 300 }}
              control={<Switch checked={adelaMode} onClick={() => setAdelaMode(!adelaMode)} />}
              label="Adela Mode (Maxed Passive)"
            />
          </Grid>
        ) : ''}
      {
        computeSortedBuildStats(
          selectedItems.filter((it) => it != null) as number[],
          adelaMode,
        ).map((stats) => (
          <Grid item key={`stat-${stats.apiName}`}>
            <Typography style={{ marginRight: 5 }}>
              {propertyIdToHumanString(stats)}
            </Typography>
          </Grid>
        ))
      }
    </Grid>
  );
}
