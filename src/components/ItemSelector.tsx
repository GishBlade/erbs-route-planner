import React, { useEffect, useState } from 'react';
import {
  Accordion, AccordionDetails, AccordionSummary,
  Box,
  Grid,
  Popover, Typography,
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { ItemIcon } from './ItemIcon';
import {
  ApiItem,
  computeBuildableAllZones,
  itemCodeToDef,
} from '../erbs-data/ERBSJsonInterfaces';
import {
  defaultFilterPaneElements,
  filterItems,
  FilterPaneElement,
  ItemFilterPane,
  ItemFilterParams,
} from './ItemFilterPane';
import sortItemList from '../sorting-utils';

export interface ItemSelectorProps {
  startingItems: number[],
  selected?: number|null,
  setSelected: (code: number|null) => void,
  hideFilter?: boolean,
  initialFilter?: ItemFilterParams,
  overrideFilter?: ItemFilterParams|null,
  expandFilterByDefault?: boolean,
  filterPaneElements?: FilterPaneElement[],
  blueBadgeNumber?: number|null|undefined,
  placeholder?: 'weapon'|'chest'|'head'|'arm'|'leg'|'trinket'|undefined,
  disabled?: boolean,
}

export function ItemSelector(
  {
    startingItems,
    selected, setSelected, hideFilter = false,
    initialFilter = { allowIncompleteItems: true },
    overrideFilter = null,
    expandFilterByDefault = true,
    filterPaneElements = defaultFilterPaneElements,
    blueBadgeNumber,
    placeholder,
    disabled = false,
  }: ItemSelectorProps,
) {
  const [anchorEl, setAnchorEl] = useState(null as any);
  const [managedFilterParams, setManagedFilterParams] = useState(initialFilter as ItemFilterParams);
  const filterParams = overrideFilter != null ? overrideFilter : managedFilterParams;
  const [filteredItems, setFilteredItems] = useState([] as ApiItem[]);

  useEffect(() => {
    const allBuildableItemCodes = computeBuildableAllZones(startingItems);
    const computedFilteredItems = filterItems(allBuildableItemCodes, filterParams)
      .map((it) => itemCodeToDef[it]);
    sortItemList(computedFilteredItems, (a) => a, false, true, true);
    setFilteredItems(computedFilteredItems);
  }, [startingItems, filterParams]);
  const open = Boolean(anchorEl);
  return (
    <div>
      <ItemIcon
        disabled={disabled}
        item={selected || 0}
        placeholder={placeholder}
        blueBadgeNumber={blueBadgeNumber}
        onClick={(event) => {
          setAnchorEl(anchorEl || event.currentTarget);
          // setPopperOpen(!popperOpen);
        }}
        onRightClick={() => {
          setSelected(null);
        }}
      />
      <Popover
        open={open}
        anchorOrigin={{ vertical: 'bottom', horizontal: 'left' }}
        id="testing-popper"
        anchorEl={anchorEl}
        onClose={() => setAnchorEl(null)}
      >
        <Box style={{ width: 400 }}>
          { hideFilter ? '' : (
            <Accordion
              style={{ maxWidth: 510 }}
              defaultExpanded={filteredItems.length === 0 || expandFilterByDefault}
            >
              <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                Filter Options
              </AccordionSummary>
              <AccordionDetails>
                <ItemFilterPane
                  filterParams={filterParams}
                  setFilterParams={(it) => setManagedFilterParams(it)}
                  filterPaneElements={filterPaneElements}
                />
              </AccordionDetails>
            </Accordion>
          )}
          <Grid container style={{ maxWidth: 510 }}>
            {filteredItems.length === 0 ? (
              <Grid item key="no-buildable" style={{ margin: 10 }}>
                <Typography>Nothing To Select</Typography>
              </Grid>
            ) : ''}
            {filteredItems.map(
              (it) => (
                <Grid item id={`selector-${it.code}`} key={`selector-${it.code}`}>
                  <ItemIcon
                    disabled={disabled}
                    item={it.code}
                    onClick={() => {
                      setSelected(it.code);
                      setAnchorEl(null);
                      // setPopperOpen(false);
                    }}
                  />
                </Grid>
              ),
            )}
          </Grid>
        </Box>
      </Popover>
    </div>
  );
}
