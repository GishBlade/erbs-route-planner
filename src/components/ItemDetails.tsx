import React from 'react';
import { Box, Grid, Typography } from '@material-ui/core';
import {
  ApiItemType,
  itemCodeToDef,
  getEnglishName,
  weaponInfo, buildsInto,
} from '../erbs-data/ERBSJsonInterfaces';
import { BuildTree } from './BuildTree';
import { BuildStatRender } from './BuildStatRender';
import { RenderIf } from './RenderIf';

export interface ItemDetailsProps {
  itemCode: number,
  debugMode: boolean,
  renderIcon: (code: number) => any,
}

export function ItemDetails(
  { itemCode, debugMode = false, renderIcon }: ItemDetailsProps,
) {
  const item = itemCodeToDef[itemCode];
  let itemSubtype = item.itemType.toString();
  if (item.weaponType != null) {
    itemSubtype = weaponInfo[item.weaponType].englishName;
  } else if (item.itemType === ApiItemType.Armor && item.armorType != null) {
    itemSubtype = item.armorType.toString();
    if (itemSubtype === 'Trinket') {
      itemSubtype = 'Accessory';
    }
  } else if (item.itemType === ApiItemType.Consume && item.consumableType != null) {
    itemSubtype = item.consumableType.toString();
    if (itemSubtype === 'Beverage') {
      itemSubtype = 'Drink';
    }
  }
  // const possiblePaths = onlyNameInTooltip ? null : cachedSingleItemRouting(item.code);
  const itemBuildsInto = buildsInto[itemCode] || [];
  return (
    <Box>
      <Typography variant="h6">
        {getEnglishName(item.code)}
        {' '}
        (
        {itemSubtype}
        {debugMode ? ` - ${item.code}` : ''}
        )
      </Typography>
      <BuildStatRender selectedItems={[itemCode]} />
      <RenderIf
        condition={itemBuildsInto.length > 0}
        whenTrue={() => (
          <>
            <Typography variant="h6">Builds Into</Typography>
            <Grid container style={{ maxWidth: 280 }}>
              {
                itemBuildsInto.map((c) => (
                  <Grid item>
                    {renderIcon(c)}
                  </Grid>
                ))
              }
            </Grid>
          </>
        )}
      />
      <Typography variant="h6">Build Tree</Typography>
      <BuildTree itemCode={itemCode} renderIcon={renderIcon} />
      {/* <RenderIf */}
      {/*  condition={!onlyNameInTooltip && possiblePaths != null} */}
      {/*  whenTrue={() => ( */}
      {/*    <> */}
      {/*      <Typography>===Sample Routes===</Typography> */}
      {/*      {possiblePaths!!.map((it) => ( */}
      {/*        <Typography> */}
      {/*          {it.map((itemId) => zoneIdToName[itemId]).join(' -> ')} */}
      {/*        </Typography> */}
      {/*      ))} */}
      {/*    </> */}
      {/*  )} */}
      {/* /> */}
    </Box>
  );
}
