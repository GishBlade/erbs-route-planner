import React from 'react';
import {
  Badge,
  IconButton,
  Typography,
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import { ItemDetails } from './ItemDetails';
import {
  ApiItem,
  itemCodeToDef,
  resolveItemImageName,
  getEnglishName,
} from '../erbs-data/ERBSJsonInterfaces';
import { PopoverTooltip } from './PopoverTooltip';

export interface ItemIconProps {
  // maxDimension: number,
  item: number,
  debugMode?: boolean,
  onClick?: ((event) => void)|null,
  onRightClick?: ((event) => void)|null,
  disableTooltip?: boolean,
  blueBadgeNumber?: number|null|undefined,
  redBadgeNumber?: number|null|undefined,
  placeholder?: 'weapon'|'chest'|'head'|'arm'|'leg'|'trinket'|undefined,
  disabled?: boolean,
}

const rarityToColor = {
  Common: '#454545',
  Uncommon: '#44644A',
  Rare: '#2C3A5C',
  Epic: '#9185C9',
  Legend: '#987231',
};

export const itemIconWidth = 40;

export function ItemIcon({
  item,
  debugMode = false,
  onClick = null,
  onRightClick = null, disableTooltip = false, blueBadgeNumber = null, redBadgeNumber = null,
  placeholder,
  disabled = false,
}: ItemIconProps) {
  const itemDef: ApiItem|undefined|null = itemCodeToDef[item];
  // eslint-disable-next-line max-len
  const bgColor = itemDef == null ? rarityToColor.Common : (rarityToColor[itemDef.itemGrade] || rarityToColor.Common);
  const maxDimension = 37;
  const containerStyle = {
    backgroundColor: bgColor,
    margin: (itemIconWidth - maxDimension) / 2,
    borderRadius: 5,
    width: maxDimension,
    height: 25,
  };
  const imageStyle = {
    maxHeight: 20,
    maxWidth: maxDimension,
  };
  const src = placeholder !== undefined && itemDef == null ? `item-icons/${placeholder}.png` : `item-icons/${resolveItemImageName(item)}.png`;

  const imageThing = (onClick != null && itemDef == null)
    && placeholder === undefined ? <AddIcon /> : (
      <img
        onError={(e: any) => { e.target.onerror = null; e.target.src = 'item-icons/unknown.png'; }}
        src={src}
        alt={getEnglishName(item)}
        style={imageStyle}
      />
    );
  const tooltip = (itemDef && !disableTooltip
    ? (
      <ItemDetails
        itemCode={item}
        debugMode={debugMode}
        renderIcon={(code) => <ItemIcon item={code} />}
      />
    )
    : <Typography>Nothing Selected</Typography>);
  let zoneBadgeValue: number|string|null = null;
  let zoneBadgeColor: 'primary'|'secondary' = 'primary';
  if (blueBadgeNumber == null && redBadgeNumber == null) {
    // then do nothing
  } else if (blueBadgeNumber == null && redBadgeNumber != null) {
    zoneBadgeValue = redBadgeNumber;
    zoneBadgeColor = 'secondary';
  } else if (blueBadgeNumber != null && redBadgeNumber == null) {
    zoneBadgeValue = blueBadgeNumber;
    zoneBadgeColor = 'primary';
  } else if (blueBadgeNumber === redBadgeNumber) {
    zoneBadgeColor = 'primary';
    zoneBadgeValue = blueBadgeNumber;
  } else {
    zoneBadgeColor = 'secondary';
    zoneBadgeValue = `${redBadgeNumber}/${blueBadgeNumber}`;
  }
  return (
    <PopoverTooltip
      title={tooltip}
    >
      <Badge
        badgeContent={zoneBadgeValue}
        color={zoneBadgeColor}
        anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      >
        <IconButton
          onClick={onClick || (() => {})}
          style={containerStyle}
          disabled={onClick == null || disabled}
          size="small"
          onContextMenu={(e) => {
            if (onRightClick != null) {
              e.preventDefault();
              onRightClick(e);
            }
          }}
        >
          {imageThing}
        </IconButton>
      </Badge>
    </PopoverTooltip>
  );
}
