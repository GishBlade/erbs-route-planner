#!/bin/bash -i

set -e

export STAGE="pre-run"
echo "sourcing profile"
source ~/.profile
export XDG_RUNTIME_DIR=/run/user/$(id -u)

#notify-send "ERBS Build Start" "STAGE: ${STAGE}"

on_error(){
  notify-send "ERBS Build Failure" "STAGE: ${STAGE}"
}

trap 'on_error' ERR


echo "running $(date)"
set -v

cd $ERBS_PROJECT_FOLDER
ORIGINAL_FOLDER=$PWD

echo "activating"
source venv/bin/activate

echo "Original folder ${ORIGINAL_FOLDER}"

cd ./src/erbs-data

export STAGE="download"
python3 download-erbs-data.py
export STAGE="refactor"
python3 api-refactor-script.py
export STAGE="process translation file"
./process-translation-file.py
export STAGE="sanity checker"
./api-data-sanity-checker.py

cd $ORIGINAL_FOLDER

export STAGE="git status"
if [[ `git status --porcelain` ]]; then
  echo "Detected changes with git status"
else
  echo "No changes detected, so returning early"
  notify-send "ERBS Build Skipped" "STAGE: ${STAGE}"
  exit 0
fi

echo "Updating update string file."
export STAGE="date update"
echo -e "// eslint-disable-next-line import/prefer-default-export\nexport const UPDATE_TIME: string = '$(date "+%B %d, %Y")';" > src/update-time.ts

export STAGE="npm build"
npm run build

export STAGE="git commit/push"
git add -A
git commit -m "Automated Git update commit"
git push -u origin master

export STAGE="netlify deploy"
netlify deploy --prod --dir=build

#notify-send "ERBS Build Success" "STAGE: ${STAGE}"

